import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import foto1 from '../images/1.jpg'
import foto2 from '../images/2.jpg'
import foto3 from '../images/3.jpg'
import foto4 from '../images/4.jpg'
import foto5 from '../images/5.jpg'
import foto6 from '../images/6.jpg'
import foto7 from '../images/7.jpg'

const styles = makeStyles(theme => ({
  judul: {
    [theme.breakpoints.down('xs')]: {
      fontSize: '7vw',
    },
    marginLeft: '4%',
    width: '92%',
    marginBottom: '20px',
  },
  content: {
    marginLeft: '4%',
    width: '92%',
    marginBottom: '20px',
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  photo: {
    width: '350px',
    height: '200px',
    marginBottom: '30px',
    [theme.breakpoints.only('xs')]: {
      width: '245px',
      height: '140px',
    },
  }
}))

const News = ({ stimulus }) => {
  const classes = styles()
  const [show, setShow] = React.useState(true);

  const judul = (stim) => {
    if (stim == 1) {
      return <b>Realisasi Investasi Asing tahun 2019: Investasi Asing Terus Meningkat</b>
    } else if (stim == 2) {
      return <b>BKPM: Investasi China Naik Pesat dibanding Negara Lain</b>
    } else if (stim == 3) {
      return <b>BKPM: Investasi Arab Saudi Naik Pesat dibanding Negara Lain</b>
    } else if (stim == 4) {
      return <b>BKPM: Investasi Amerika Serikat Naik Pesat dibanding Negara Lain</b>
    } else if (stim == 5) {
      return <b>China Perluas Pengaruhnya Melalui Investasi Asing</b>
    } else if (stim == 6) {
      return <b>Arab Saudi Perluas Pengaruhnya Melalui Investasi Asing</b>
    } else if (stim == 7) {
      return <b>Amerika Serikat Perluas Pengaruhnya Melalui Investasi Asing</b>
    }
  }

  const foto = ( stim ) => {
    if (stim == 1) {
      return <img src={foto1} className={classes.photo} alt='stimulus-pic' />
    } else if (stim == 2) {
      return <img src={foto2} className={classes.photo} alt='stimulus-pic' />
    } else if (stim == 3) {
      return <img src={foto3} className={classes.photo} alt='stimulus-pic' />
    } else if (stim == 4) {
      return <img src={foto4} className={classes.photo} alt='stimulus-pic' />
    } else if (stim == 5) {
      return <img src={foto5} className={classes.photo} alt='stimulus-pic' />
    } else if (stim == 6) {
      return <img src={foto6} className={classes.photo} alt='stimulus-pic' />
    } else if (stim == 7) {
      return <img src={foto7} className={classes.photo} alt='stimulus-pic' />
    }
  }

  const isi = (stim) => {
    if (stim == 1) {
      return <p>Badan Koordinasi Penanaman Modal (BKPM) mencatat realisasi investasi asing atau penanaman modal asing (PMA) ke Tanah Air sepanjang kuartal IV / 2019 sebesar Rp.423,1 triliun dibandingkan dengan sebelumnya yang hanya sebesar Rp.392,7 triliun atau tumbuh 23,17 persen. Berdasarkan bidang usaha, investasi di bidang listrik, gas, dan air menempati urutan pertama dan disusul oleh bidang transportasi, gudang dan telekomunikasi; dan industri logam dasar, barang logam bukan mesin. Selain itu, analis keuangan menilai terdapat minat yang kuat dari perusahaan yang bergerak di sektor kendaraan listrik, sektor logistik, dan sektor pergudangan. Ketiga sektor itu, lanjutnya, yang akan menopang permintaan lahan industri beberapa tahun ke depan.</p>
    } else if (stim == 2) {
      return <p>Badan Koordinasi Penanaman Modal (BKPM) mencatat realisasi penanaman modal asing (PMA) yang berasal dari Negara China tumbuh pesat dengan nilai investasi Rp.64,7 triliun dari posisi tahun lalu yang hanya Rp.33 triliun. Hal ini disebabkan China lebih agresif berinvestasi di Indonesia dibandingkan dengan negara lain.<br /><br />Meski demikian, kenaikan investasi China juga diiringi dengan kenaikan jumlah hutang luar negeri Indonesia yang kian menumpuk. Jika hutang terus dibiarkan menumpuk dan tidak dapat dibayar sebelum jatuh tempo, konsekuensi yang ditanggung negara bisa fatal. Sebut saja mengganti mata uang Rupiah menjadi Yuan atau harus merelakan sebagian wilayah kepada China.</p>
    } else if (stim == 3) {
      return <p>Badan Koordinasi Penanaman Modal (BKPM) mencatat realisasi penanaman modal asing (PMA) yang berasal dari Negara Arab Saudi tumbuh pesat dengan nilai investasi Rp.64,7 triliun dari posisi tahun lalu yang hanya Rp.33 triliun. Hal ini disebabkan Arab Saudi lebih agresif berinvestasi di Indonesia dibandingkan dengan negara lain.<br /><br />Meski demikian, kenaikan investasi Arab Saudi juga diiringi dengan kenaikan jumlah hutang luar negeri Indonesia yang kian menumpuk. Jika hutang terus dibiarkan menumpuk dan tidak dapat dibayar sebelum jatuh tempo, konsekuensi yang ditanggung negara bisa fatal. Sebut saja mengganti mata uang Rupiah menjadi Riyal atau harus merelakan sebagian wilayah kepada Arab Saudi.</p>
    } else if (stim == 4) {
      return <p>Badan Koordinasi Penanaman Modal (BKPM) mencatat realisasi penanaman modal asing (PMA) yang berasal dari Negara Amerika Serikat tumbuh pesat dengan nilai investasi Rp.64,7 triliun dari posisi tahun lalu yang hanya Rp33 triliun. Hal ini disebabkan Amerika Serikat lebih agresif berinvestasi di Indonesia dibandingkan dengan negara lain.<br /><br />Meski demikian, kenaikan investasi Amerika Serikat juga diiringi dengan kenaikan jumlah hutang luar negeri Indonesia yang kian menumpuk. Jika hutang terus dibiarkan menumpuk dan tidak dapat dibayar sebelum jatuh tempo, konsekuensi yang ditanggung negara bisa fatal. Sebut saja mengganti mata uang Rupiah menjadi Dollar atau harus merelakan sebagian wilayah kepada Amerika Serikat.</p>
    } else if (stim == 5) {
      return <p>"Sudah jadi tradisi China menegakkan pengaruhnya melalui pemberian uang atau janji dalam bentuk investasi asing", kata seorang pakar China dari Yayasan Ilmu Pengetahuan dan Politik. Masuknya investasi asing tidak hanya berdampak pada perekonomian negara, tetapi juga merambah ke bidang pendidikan, budaya dan gaya hidup. Sejak awal era Orde Baru, negara-negara asing seperti China memang telah mencengkeram negeri ini dalam berbagai bidang.<br /><br />China misalnya mulai banyak mendanai para sarjana dari Indonesia untuk melanjutkan studi di Universitas-Universitas di China. Namun hal ini menjadi kedok untuk menyebarkan ajaran-ajaran pandangan ala komunisme China. Peningkatan pandangan ini dapat memicu polarisasi dan konflik. Beberapa khawatir bahwa peningkatan “Cinaisasi” Indonesia telah menyebabkan meningkatnya intoleransi dan perpecahan. Ini memiliki dampak yang lebih kuat ketika China tidak hanya mengekspor pandangan Komunisme, tetapi kemudian mendorong orang-orang untuk memasuki politik.</p>
    } else if (stim == 6) {
      return <p>"Sudah jadi tradisi Arab Saudi menegakkan pengaruhnya melalui pemberian uang atau janji dalam bentuk investasi asing", kata seorang pakar Arab Saudi dari Yayasan Ilmu Pengetahuan dan Politik. Masuknya investasi asing tidak hanya berdampak pada perekonomian negara, tetapi juga merambah ke bidang pendidikan, budaya dan gaya hidup. Sejak awal era Orde Baru, negara-negara asing seperti Arab Saudi memang telah mencengkeram negeri ini dalam berbagai bidang.<br /><br />Arab Saudi misalnya mulai banyak mendanai para sarjana dari Indonesia untuk melanjutkan studi di Universitas-Universitas di Arab Saudi. Namun hal ini menjadi kedok untuk menyebarkan ajaran-ajaran pandangan Wahabisme Arab Saudi. Peningkatan pandangan ini dapat memicu polarisasi dan konflik. Beberapa khawatir bahwa peningkatan “Arabisasi” Indonesia telah menyebabkan meningkatnya intoleransi dan perpecahan. Ini memiliki dampak yang lebih kuat ketika Arab Saudi tidak hanya mengekspor pandangan Wahabisme, tetapi kemudian mendorong orang-orang untuk memasuki politik.</p>
    } else if (stim == 7) {
      return <p>"Sudah jadi tradisi Amerika menegakkan pengaruhnya melalui pemberian uang atau janji dalam bentuk investasi asing", kata seorang pakar Amerika Serikat dari Yayasan Ilmu Pengetahuan dan Politik. Masuknya investasi asing tidak hanya berdampak pada perekonomian negara, tetapi juga merambah ke bidang pendidikan, budaya dan gaya hidup. Sejak awal era Orde Baru, negara-negara asing seperti Amerika Serikat memang telah mencengkeram negeri ini dalam berbagai bidang.<br /><br />Amerika Serikat misalnya mulai banyak mendanai para sarjana dari Indonesia untuk melanjutkan studi di Universitas-Universitas di Amerika Serikat. Namun hal ini menjadi kedok untuk menyebarkan ajaran-ajaran pandangan kapitalisme Amerika Serikat. Peningkatan pandangan ini dapat memicu polarisasi dan konflik. Beberapa khawatir bahwa peningkatan “Amerikanisasi” Indonesia telah menyebabkan meningkatnya intoleransi dan perpecahan. Ini memiliki dampak yang lebih kuat ketika Amerika Serikat tidak hanya mengekspor pandangan Kapitalisme, tetapi kemudian mendorong orang-orang untuk memasuki politik.</p>
    }
  }
  return (
    <div>
      <Typography variant='h4' align='center' className={classes.judul}>
        {judul(stimulus)}
      </Typography>
      <div className={classes.center}>
        {foto(stimulus)}
      </div>
      <Typography className={classes.content}>
        {isi(stimulus)}
      </Typography>
    </div>
  )
}
export default News