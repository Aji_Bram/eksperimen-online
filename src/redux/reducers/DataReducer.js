const stimState = {
  stimulus: null
};

const penil1State = {
  menarik: null,
  koheren: null,
  lengkap: null
};

const penil2State = {
  one: null,
  two: null,
  three: null,
  four: null,
  five: null,
  six: null,
  seven: null,
  eight: null,
  nine: null,
  ten: null,
  eleven: null,
  twelve: null,
  thirteen: null,
  fourteen: null,
  fifteen: null,
  sixteen: null,
  seventeen: null,
  eighteen: null,
  nineteen: null,
  twenty: null,
  twentyone: null,
  twentytwo: null
};

const penil3State = {
  sifatOne: null,
  sifatTwo: null,
  sifatThree: null,
  sifatFour: null,
  sifatFive: null,
  sifatSix: null,
  sifatSeven: null,
  sifatEight: null,
  sifatNine: null,
  sifatTen: null
};

const penil4State = {
  sitCemas: null,
  sitRagu: null,
  sitKhawatir: null,
  sitCanggung: null,
  sitGugup: null,
  sitTerancam: null
};

const penil5State = {
  fokusOne: null,
  fokusTwo: null,
  fokusCountry: null
};

const infoState = {
  name: null,
  age: null,
  religion: null,
  etnic: null,
  gender: null,
  address: null,
  lastEdu: null,
  job: null,
  fokusAwal: null,
}

export const stim = (state = stimState, action) => {
  switch (action.type) {
    case 'STIM':
      return{...state, stimulus: action.stimulus}
    default:
      return state
  }
}

export const penil1 = (state = penil1State, action) => {
  switch (action.type) {
    case 'PENIL1':
      return { ...state, menarik: action.menarik, koheren: action.koheren, lengkap: action.lengkap }
    default:
      return state
  }
}

export const penil2 = (state = penil2State, action) => {
  switch (action.type) {
    case 'PENIL2':
      return { ...state, one: action.one, two: action.two, three: action.three, four: action.four, five: action.five, six: action.six, seven: action.seven, eight: action.eight, nine: action.nine, ten: action.ten, eleven: action.eleven, twelve: action.twelve, thirteen: action.thirteen, fourteen: action.fourteen, fifteen: action.fifteen, sixteen: action.sixteen, seventeen: action.seventeen, eighteen: action.eighteen, nineteen: action.nineteen, twenty: action.twenty, twentyone: action.twentyone, twentytwo: action.twentytwo }
    default:
      return state
  }
}

export const penil3 = (state = penil3State, action) => {
  switch (action.type) {
    case 'PENIL3':
      return { ...state, sifatOne: action.sifatOne, sifatTwo: action.sifatTwo, sifatThree: action.sifatThree, sifatFour: action.sifatFour, sifatFive: action.sifatFive, sifatSix: action.sifatSix, sifatSeven: action.sifatSeven, sifatEight: action.sifatEight, sifatNine: action.sifatNine, sifatTen: action.sifatTen}
    default:
      return state
  }
}

export const penil4 = (state = penil4State, action) => {
  switch (action.type) {
    case 'PENIL4':
      return { ...state, sitCemas: action.sitCemas, sitRagu: action.sitRagu, sitKhawatir: action.sitKhawatir, sitCanggung: action.sitCanggung, sitGugup: action.sitGugup, sitTerancam: action.sitTerancam}
    default:
      return state
  }
}

export const penil5 = (state = penil5State, action) => {
  switch (action.type) {
    case 'PENIL5':
      return { ...state, fokusOne: action.fokusOne, fokusTwo: action.fokusTwo, fokusCountry: action.fokusCountry}
    default:
      return state
  }
}

export const info = (state = infoState, action) => {
  switch (action.type) {
    case 'INFO':
      return { ...state, name: action.name, age: action.age, religion: action.religion, etnic: action.etnic, gender: action.gender, address: action.address, lastEdu: action.lastEdu, job: action.job, fokusAwal: action.fokusAwal}
    default:
      return state
  }
}
