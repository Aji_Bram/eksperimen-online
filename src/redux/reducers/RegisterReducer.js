const initialState = {
    fetching: false,
    fetched: false,
    error: null,
    data: [],
    status:null
}
const initialProv = {
    fetching:false,
    fetched:false,
    error:null,
    data:[],
}

  export const getProv = (state=initialProv, action)=>{
      switch(action.type){
          case 'GET_PROV_PENDING':
              return{...state, fetching: true, error:null}
          case 'GET_PROV_FULFILLED':
              return{...state, fetching: false, fetched: true, data: action.payload.data.data}
          case 'GET_PROV_REJECTED':
              return{...state, fetching: false, error: action.payload.data}
          default:
              return state

      }
  }
  
  export const register=(state = initialState, action)=>{
    switch (action.type) {
      case 'REGISTER_PENDING':
        return{...state, fetching: true, error:null}
      case 'REGISTER_FULFILLED':
        return{...state, fetching: false, fetched: true, data: action.payload.data, status:action.payload.data}
      case 'REGISTER_REJECTED':
        return{...state, fetching: false, error: action.payload.data}
      default:
        return state
    }
  }

  export const setNewPassword=(state = initialState, action) => {
    switch (action.type){
      case 'SETNEWPASSWORD_PENDING':
        return{...state, fetching: true, error:null}
      case 'SETNEWPASSWORD_FULFILLED':
        return{...state, fetching: false, fetched: true, data: action.payload.data}
      case 'SETNEWPASSWORD_REJECTED':
        return{...state, fetching: false, error: action.payload.data}
      default:
        return state
    }
  }
  export const getEmail = (state=initialState,action)=>{
    switch (action.type) {
        case 'EMAIL_PENDING':
            return{...state, fetching:true, error:null}
        case 'EMAIL_FULLFILLED':
            return{...state, fetching:false,fetched:true,data:action.payload.data}
        case 'EMAIL_REJECTED':
            return{...state, fetching:false,error:action.payload.data}
        default:
            return state
    }
  }
  export const getPassword = (state=initialState,action)=>{
    switch (action.type) {
        case 'PASSWORD_PENDING':
            return{...state, fetching:true, error:null}
        case 'PASSWORD_FULLFILLED':
            return{...state, fetching:false,fetched:true,data:action.payload.data}
        case 'PASSWORD_REJECTED':
            return{...state, fetching:false,error:action.payload.data}
        default:
            return state
    }
  }
  
  
  export const username=(state = initialState, action)=>{
    switch (action.type) {
      case 'USERNAME_PENDING':
        return{...state, fetching: true, error:null}
      case 'USERNAME_FULFILLED':
        return{...state, fetching: false, fetched: true, message: action.payload.data, status:action.payload.data}
      case 'USERNAME_REJECTED':
        return{...state, fetching: false, error: action.payload.data, status:action.payload.data}
      default:
        return state
    }
  }

  export const email=(state = initialState, action)=>{
    switch (action.type) {
      case 'EMAIL_PENDING':
        return{...state, fetching: true, error:null}
      case 'EMAIL_FULFILLED':
        return{...state, fetching: false, fetched: true, message: action.payload.data, status:action.payload.data}
      case 'EMAIL_REJECTED':
        return{...state, fetching: false, error: action.payload.data, status:action.payload.data}
      default:
        return state
    }
  }

  export const handphone=(state = initialState, action)=>{
    switch (action.type) {
      case 'HANDPHONE_PENDING':
        return{...state, fetching: true, error:null}
      case 'HANDPHONE_FULFILLED':
        return{...state, fetching: false, fetched: true, message: action.payload.data, status:action.payload.data}
      case 'HANDPHONE_REJECTED':
        return{...state, fetching: false, error: action.payload.data, status:action.payload.data}
      default:
        return state
    }
  }

  export const getDetailItem = (state= initialState, action)=>{
    switch(action.type){
        case 'GET_DETAIL_ITEM_PENDING':
            return{...state, fetching: true, error:null}
        case 'GET_DETAIL_ITEM_FULFILLED':
            return{...state, fetching: false, fetched: true, data: action.payload.data.data}
        case 'GET_DETAIL_ITEM_REJECTED':
            return{...state, fetching: false, error: action.payload.data}
        default:
            return state

    }
  }
