import axios from 'axios';
import {pingAuction} from '../../config';

const headers = {
    'Content-Type':'application/json',
    'Accept': 'application/json',
}

export function getProv(){
    return{
        type:'GET_PROV',
        payload:axios.get(`${pingAuction}get-prov`)

    }
}

export function register(username, password, id_prov, email, handphone){
    return{
      type: 'REGISTER',
      payload: axios.post(`${pingAuction}register`,{
        username,
        password,
        id_prov,
        email,
        handphone,

      },{
        headers
      })
    }
}

export function setNewPassword(password1, password2){
  return{
    type: 'setNewPassword',
    payload: axios.post(`${pingAuction}set-new-password/:token`,{
      password1,
      password2,

    },{
      headers
    })
  }
}
  export function getEmail(email){
    return{
        type:'FORGETPASS',
        payload: axios.post(`${pingAuction}forgot-password`,{
            email,
        },{
            headers
        })     
    }
  }

  export function getPassword(password,token){
    return{
      type:'PASSWORD',
      payload: axios.post(`${pingAuction}set-new-password/:token`,{
        password, token
      },{
        headers
      })
    }
  }

  export function getDetailItem(){
    return{
      type:'GET_DETAIL_ITEM',
      payload:axios.get(`${pingAuction}item-detail`)

    }
  }

  
