const head = {
  'Content-Type': 'application/json',
  'Accept': 'application/json',
}

export function stim(stimulus) {
  return {
    type: 'STIM',
    stimulus: stimulus,
  }
}

export function penil1(menarik, koheren, lengkap) {
  return {
    type: 'PENIL1',
    menarik: menarik,
    koheren: koheren,
    lengkap: lengkap,
  }
}

export function penil2(one, two, three, four, five, six, seven, eight, nine, ten, eleven, twelve, thirteen, fourteen, fifteen, sixteen, seventeen, eighteen, nineteen, twenty, twentyone, twentytwo) {
  return {
    type: 'PENIL2',
    one: one,
    two: two,
    three: three,
    four: four,
    five: five,
    six: six,
    seven: seven,
    eight: eight,
    nine: nine,
    ten: ten,
    eleven: eleven,
    twelve: twelve,
    thirteen: thirteen,
    fourteen: fourteen,
    fifteen: fifteen,
    sixteen: sixteen,
    seventeen: seventeen,
    eighteen: eighteen,
    nineteen: nineteen,
    twenty: twenty,
    twentyone: twentyone,
    twentytwo: twentytwo
  }
}

export function penil3(sifatOne, sifatTwo, sifatThree, sifatFour, sifatFive, sifatSix, sifatSeven, sifatEight, sifatNine, sifatTen) {
  return {
    type: 'PENIL3',
    sifatOne: sifatOne,
    sifatTwo: sifatTwo,
    sifatThree: sifatThree,
    sifatFour: sifatFour,
    sifatFive: sifatFive,
    sifatSix: sifatSix,
    sifatSeven: sifatSeven,
    sifatEight: sifatEight,
    sifatNine: sifatNine,
    sifatTen: sifatTen
  }
}

export function penil4(sitCemas, sitRagu, sitKhawatir, sitCanggung, sitGugup, sitTerancam) {
  return {
    type: 'PENIL4',
    sitCemas: sitCemas,
    sitRagu: sitRagu,
    sitKhawatir: sitKhawatir,
    sitCanggung: sitCanggung,
    sitGugup: sitGugup,
    sitTerancam: sitTerancam,
  }
}

export function penil5(fokusOne, fokusTwo, fokusCountry) {
  return {
    type: 'PENIL5',
    fokusOne: fokusOne,
    fokusTwo: fokusTwo,
    fokusCountry: fokusCountry,
  }
}

export function info(name, age, religion, etnic, gender, address, lastEdu, job, fokusAwal) {
  return {
    type: 'INFO',
    name: name,
    age: age,
    religion: religion,
    etnic: etnic,
    gender: gender,
    address: address,
    lastEdu: lastEdu,
    job: job,
    fokusAwal: fokusAwal,
  }
}