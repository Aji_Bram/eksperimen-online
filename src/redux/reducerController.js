import { stim, penil1, penil2, penil3, penil4, penil5, info } from './reducers/DataReducer';
import { sessionReducer } from 'redux-react-session'

export default { stim, penil1, penil2, penil3, penil4, penil5, info, session:sessionReducer }
