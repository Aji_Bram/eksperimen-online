const helper = require('../../helpers');
var connection = require('../../connection');
const fs = require('fs');

exports.createBarang = function (req, res) {
  jwt.verify(req.token, config.secretkey, async (err, authData) => {
    if (err) {
      res.json({
        message: "invalid token!"
      });
    } else {

      const username = authData.username
      const file = req.files;
      const judul = req.body.judul;
      const deskripsi = req.body.deskripsi;
      const harga_awal = req.body.harga_awal;
      const harga_akhir = req.body.harga_akhir;
      var masa_lelang = req.body.masa_lelang;
      const kelipatan_bid = req.body.kelipatan_bid;
      const isNew = req.body.isNew;
      const merk = req.body.merk;
      const id_kategori = req.body.id_kategori;
      const jumlah_view = 0;
      const date = dateFormat(new Date());
      const status = 'tersedia';

      let url = []
      var setValue = function (value) {
        url.push(value)
      }
      // masa_lelang = dateFormat(masa_lelang)
      // console.log(masa_lelang)
      await Promise.all(file.map(async (element) => {
        var params = {
          Bucket: BUCKET_NAME,
          Key: randtoken.uid(50),
          Body: element.buffer,
          ContentType: element.mimetype,
          ACL: 'public-read'
        };
        await new Promise((resolve, reject) => {
          s3.upload(params, function (err, data) {
            if (err) {
              throw err
            }
            setValue(data.Location)
            resolve()
          })
        })
      }))

      let query = `SELECT * FROM USERS where username='${username}';`
      var val = []
      var setVal = function (value) {
        val = value;
      }
      connection.query(query, (error, results) => {
        if (error) {
          res.send({
            message: error,
            status: helper.status.error
          })
          res.end()
        }
        else {
          setVal(results.rows[0]);
          const id_penjual = val.id_user
          const id_prov = val.id_prov
          let query_prov = `SELECT nama_prov from provinsi where id_prov=${id_prov};`
          connection.query(query_prov, (error, results) => {
            if (error) {
              res.send({
                message: error,
                status: helper.status.error

              })
              res.end()
            }
            else {
              const lokasi = results.rows[0].nama_prov

              let createBarang = `INSERT INTO BARANG(judul, deskripsi, harga_awal, harga_akhir,lokasi,masa_lelang, kelipatan_bid,isNew,merk,jumlah_view,created_date,status,id_penjual, id_kategori) 
                                VALUES ('${judul}','${deskripsi}', ${harga_awal},${harga_akhir},'${lokasi}','${masa_lelang}',ARRAY[${kelipatan_bid}],'${isNew}','${merk}',${jumlah_view},'${date}','${status}',${id_penjual},${id_kategori}) RETURNING *;`

              connection.query(createBarang, async (error, results) => {
                if (error) {
                  res.send({
                    message: error,
                    status: helper.status.error
                  })
                  res.end()
                }
                else {
                  const id_barang = results.rows[0].id_barang

                  await Promise.all(url.map(async (data) => {
                    let insertGambar = `INSERT INTO IMG(id_barang,url) VALUES(${id_barang},'${data}');`

                    await new Promise((resolve, reject) => {
                      connection.query(insertGambar, (error, results) => {
                        if (error) {
                          res.send({
                            message: error,
                            status: helper.status.error
                          })
                          res.end()
                        }
                        resolve()
                      })
                      connection.end()
                    })
                  }))
                  res.send({
                    message: 'success',
                    status: helper.status.success
                  })
                  res.end()
                }
              })
              connection.end()
            }
          })
          connection.end()
        }
      })
      connection.end()
    }
  });
}