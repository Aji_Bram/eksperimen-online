import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom'
import { Button, Typography, Paper, FormGroup, FormControlLabel, Grid, Checkbox, Table, TableContainer, TableBody, TableRow, TableCell, TextField, Box } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '5% 32% 3% 22%',
    [theme.breakpoints.down('md')]: {
      padding: '5% 22% 2% 8%',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5% 20% 2% 5%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    width: '100%',
    marginBottom: '20px',
  },
  buttonChoose: {
    padding: "5px 35px",
  },
  button: {
    backgroundColor: theme.palette.primary.main,
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#090a3a',
      color: '#ebebeb',
    },
  },
  right: {
    paddingRight: '4%',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  left: {
    paddingLeft: '2%',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  table: {
    marginBottom: '15px'
  },
}))

const Penilaian5 = () => {
  const classes = styles();

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])

  const negara = ["Pulsa", "Saldo GoPay", "Saldo OVO", "Saldo DANA", "Saldo LinkAja", "Lainnya"]
  const [values, setValues] = React.useState({
    phone: '',
    method: '',
    other: '',
    fix: '',
    valid: false
  });
  const [redirect, setRedirect] = React.useState(false);

  const handleChange = (opt) => event => {
    const { value } = event.target

    if (opt === 'phone') {
      var nums = /^[0-9\b]+$/;
      if (nums.test(value) || value === '') {
        if (values.method !== '' && value !== '') {
          if (values.method === 'Lainnya') {
            if (values.other !== '') {
              setValues(prev => ({ ...prev, phone: value, valid: true }))
            } else {
              setValues(prev => ({ ...prev, phone: value, valid: false }));
            }
          } else {
            setValues(prev => ({ ...prev, phone: value, valid: true }));
          }
        } else {
          setValues(prev => ({ ...prev, phone: value, valid: false }));
        }
      }
    } else if (opt === 'other') {
      if (values.phone !== '') {
        if (value === '') {
          setValues(prev => ({ ...prev, other: value, fix: values.method, valid: false }));
        } else {
          setValues(prev => ({ ...prev, other: value, fix: value, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, other: value, fix: value, valid: false }));
      }
    }
  };

  const handleOnChange = (opt, param) => {
    if (opt === 'method') {
      if (values.phone !== '') {
        if (param === "Lainnya") {
          if (values.other === '') {
            setValues(prev => ({ ...prev, method: param, fix: param, valid: false }));
          } else {
            setValues(prev => ({ ...prev, method: param, fix: values.other, valid: true }));
          }
        } else {
          setValues(prev => ({ ...prev, method: param, fix: param, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, method: param, fix: param, valid: false }));
      }
    }
  };

  // const sendData = async e => {
  //   e.preventDefault();
  //   try {
  //     const response = await fetch(
  //       "https://v1.nocodeapi.com/ferrari689/google_sheets/wYUAERVmpKywqSvf?tabId=Undian",
  //       {
  //         method: "post",
  //         body: JSON.stringify([[values.phone, values.fix]]),
  //         headers: {
  //           "Content-Type": "application/json"
  //         }
  //       }
  //     );
  //     const json = await response.json();
  //     console.log("Success:", JSON.stringify(json));
  //     setRedirect(true);
  //   } catch (error) {
  //     console.error("Error:", error);
  //   }
  // };

  const sendData = () => {
    setRedirect(true);
  };

  const renderRedirect = () => {
    if (redirect === true) {
      return <Redirect to='/thanks-1' />
    }
  }

  return (
    <Fragment>
      {renderRedirect()}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='justify' className={classes.content}>
              Kompensasi penelitian diberikan dalam bentuk undian hadiah pulsa/saldo uang-el 
              sebesar Rp20.000,00 untuk 30 orang terpilih. Undian akan dilakukan setelah proses pengumpulan data selesai (kurang dari 1 bulan).
            </Typography>
            <Typography align='justify' className={classes.content}>
              Undian berlaku bagi seluruh individu yang terlibat dalam penelitian yakni partisipan yang menyelesaikan kuesioner maupun partisipan 
              yang menarik diri atau dikeluarkan dari analisis penelitian karena alasan tertentu. Karena bentuk kompensasi adalah uang elektronik, 
              untuk mengikuti undian, partisipan perlu mencantumkan nomor ponsel yang bisa dihubungi. Peneliti akan mengacak nomor-nomor ponsel yang 
              telah terdaftar untuk mendapatkan 30 nomor yang terpilih. Pengacakan dilakukan dengan bantuan web pengacak. Pemberitahuan pemenang 
              dilakukan melalui pesan teks dan pengiriman langsung pulsa/saldo uang elektronik ke nomor pemenang sesuai dengan pilihan pemenang 
              saat mengisi kuesioner.
            </Typography>
            <Typography align='justify' className={classes.content}>
              Jika Anda bersedia mengikuti undian, mohon cantumkan nomor ponsel Anda dan pilihan hadiah Anda
            </Typography>
            <Grid container>
              <Grid item xs={12} md={2}>
                <Typography>No. Ponsel:</Typography>
              </Grid>
              <Grid item xs={12} md={10}>
                <TextField id="standard-basic" placeholder="08xxxxxxxxx" style={{ width: "100%" }} value={values.phone} onChange={handleChange('phone')} InputLabelProps={{ shrink: true, }} />
              </Grid>
            </Grid>
            
            <FormGroup style={{ marginLeft: "2%" }}>
              {negara.map((el) =>
                <FormControlLabel
                  control={<Checkbox checked={values.method === el} onClick={() => handleOnChange('method', el)} name="method" color="primary" />}
                  label={el}
                />
              )}
            </FormGroup>
            <Box display={values.method === 'Lainnya' ? "block" : "none"}>
              <TextField id="standard-basic" placeholder="Lainnya" style={{ width: "40%", paddingLeft: '6%', marginBottom: '3%' }} onChange={handleChange('other')} InputLabelProps={{ shrink: true, }} />
            </Box>
            <Typography align='justify' className={classes.content}>
              Data ini hanya akan disimpan peneliti selama penelitian berlangsung dan akan langsung memusnahkannya setelah undian selesai dilakukan
            </Typography>
            <div className={classes.right}>
              <Button disabled={!values.valid} variant="contained" size="medium" onClick={sendData} className={classes.button}>
                <b>Submit </b><SendIcon fontSize='small' style={{ marginLeft: "6px" }} />
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Penilaian5