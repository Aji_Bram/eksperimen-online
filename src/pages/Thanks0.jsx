import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper } from '@material-ui/core';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    backgroundSize: 'auto',
    width: '100%',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '15% 32% 3% 22%',
    [theme.breakpoints.down('sm')]: {
      padding: '15% 22% 3% 12%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    paddingLeft: '10%',
    width: '80%',
    marginBottom: '20px'
  },
}))

const Thanks0 = () => {
  const classes = styles();

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])
  
  return (
    <Fragment>
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='center' className={classes.content}>Terima kasih atas waktu yang Anda berikan.<br/>
              Jika Anda berubah pikiran dan tertarik menjadi partisipan, Anda bisa kembali ke tautan awal selama penelitian masih berlangsung.
            </Typography>
            <ThumbUpAltOutlinedIcon fontSize='large' style={{ marginLeft: '47%' }} />
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Thanks0