import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom'
import { Button, Slider, Typography, Paper, Radio, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, RadioGroup, FormControlLabel, Box } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useDispatch } from 'react-redux';
import { penil4 } from '../redux/actions/DataAction';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '3% 25% 2% 13%',
    [theme.breakpoints.down('md')]: {
      padding: '3% 22% 2% 8%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    width: '100%',
    marginBottom: '20px',
  },
  button: {
    marginTop: '4%',
    backgroundColor: theme.palette.primary.main,
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#090a3a',
      color: '#ebebeb',
    },
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
}))

const Penilaian4 = () => {
  const classes = styles();
  const dispatch = useDispatch();
  const label = [["Cemas", "one"], ["Tidak Yakin; ragu", "two"], ["Khawatir", "three"], ["Canggung", "four"], ["Gugup", "five"], ["Terancam", "six"]];
  const sifat = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const [value, setValue] = React.useState({
    one: 0,
    two: 0,
    three: 0,
    four: 0,
    five: 0,
    six: 0,
    valid: false,
  });
  const marks = [
    { value: 1, label: '1', },
    { value: 2, label: '2', },
    { value: 3, label: '3', },
    { value: 4, label: '4', },
    { value: 5, label: '5', },
    { value: 6, label: '6', },
    { value: 7, label: '7', },
    { value: 8, label: '8', },
    { value: 9, label: '9', },
    { value: 10, label: '10', },
  ];
  const [redirect, setRedirect] = React.useState(false);

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])

  const handleSliderChange = (name) => (event, newValue) => {
    const val = newValue;

    if (name === 'one') {
      if (value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, one: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, one: val }));
      }
    } else if (name === 'two') {
      if (value.one !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, two: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, two: val }));
      }
    } else if (name === 'three') {
      if (value.one !== 0 && value.two !== 0 && value.four !== 0 && value.five !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, three: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, three: val }));
      }
    } else if (name === 'four') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.five !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, four: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, four: val }));
      }
    } else if (name === 'five') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, five: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, five: val }));
      }
    }
    else if (name === 'six') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0) {
        setValue(prev => ({ ...prev, six: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, six: val }));
      }
    }
  };

  const handleOnChange = (event) => {
    const val = event.target.value;

    if (event.target.name === 'one') {
      if (value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, one: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, one: val }));
      }
    } else if (event.target.name === 'two') {
      if (value.one !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, two: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, two: val }));
      }
    } else if (event.target.name === 'three') {
      if (value.one !== 0 && value.two !== 0 && value.four !== 0 && value.five !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, three: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, three: val }));
      }
    } else if (event.target.name === 'four') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.five !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, four: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, four: val }));
      }
    } else if (event.target.name === 'five') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.six !== 0) {
        setValue(prev => ({ ...prev, five: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, five: val }));
      }
    }
    else if (event.target.name === 'six') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0) {
        setValue(prev => ({ ...prev, six: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, six: val }));
      }
    }
  };

  const handleSubmit = (a, b, c, d, e, f) => {
    dispatch(penil4(a, b, c, d, e, f));
    setRedirect(true);
  }

  const renderRedirect = () => {
    if (redirect === true) {
      return <Redirect to='/penilaian-5' />
    }
  }

  return (
    <Fragment>
      {renderRedirect()}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='justify' className={classes.content}>
              Selanjutnya, Anda diminta untuk membayangkan situasi ketika Anda berinteraksi dengan investor asing. 
              Beri rating 1-10 (1=sangat tidak sesuai; 10=sangat sesuai) pada perasaan yang Anda rasakan saat berinteraksi dengan investor asing:
            </Typography>
            <Box display={{ xs: 'block', sm: 'block', md: 'none' }}>
              {label.map((element) => {
                return (
                  <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                      <Typography className={classes.heading}>{element[0]}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <Slider
                        value={element[1] === "one" ? value.one : element[1] === "two" ? value.two : element[1] === "three" ? value.three : element[1] === "four" ? value.four : element[1] === "five" ? value.five : value.six}
                        min={0}
                        step={1}
                        max={10}
                        onChange={handleSliderChange(element[1])}
                        valueLabelDisplay="auto"
                        aria-labelledby="non-linear-slider"
                        marks={marks}
                      />
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                )
              })}
            </Box>
            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
              <TableContainer className={classes.table} component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell style={{color:'#FFFFFF'}}>............................</TableCell>
                      {sifat.map((el) => 
                        <TableCell align="center">{el}</TableCell>
                      )}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow key='questionOne'>
                      <TableCell component="th" scope="row">
                        Cemas
                      </TableCell>
                      {sifat.map((el) => 
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.one == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="one"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionTwo'>
                      <TableCell component="th" scope="row">
                        Tidak Yakin; ragu
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.two == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="two"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionThree'>
                      <TableCell component="th" scope="row">
                        Khawatir
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.three == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="three"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionFour'>
                      <TableCell component="th" scope="row">
                        Canggung
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.four == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="four"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionFive'>
                      <TableCell component="th" scope="row">
                        Gugup
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.five == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="five"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionSix'>
                      <TableCell component="th" scope="row">
                        Terancam
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.six == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="six"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            <div className={classes.right}>
              <Button disabled={!value.valid} variant="contained" size="medium" onClick={() => {
                handleSubmit(value.one, value.two, value.three, value.four, value.five, value.six)}}
                className={classes.button}>
                <b>Lanjut</b>
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Penilaian4