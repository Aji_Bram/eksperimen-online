import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom';
import { Button, Typography, Paper, TableHead, FormControlLabel, Radio, RadioGroup, Table, TableContainer, TableBody, TableRow, TableCell, Box } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { penil1 } from '../redux/actions/DataAction';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '3% 28% 2% 16%',
    [theme.breakpoints.down('sm')]: {
      padding: '7% 22% 2% 8%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    marginLeft: '2%',
    width: '100%',
    marginBottom: '7px',
  },
  button: {
    marginRight: '4%',
    backgroundColor: theme.palette.primary.main,
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#090a3a',
      color: '#ebebeb',
    },
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  table: {
    marginBottom: '15px'
  },
  mini: {
    marginBottom: '15px',
    paddingLeft: '4%',
  },
}))

const Penilaian1 = () => {
  const classes = styles();
  const dispatch = useDispatch();
  const nilai = [0, 1, 2, 3, 4, 5, 6];
  const menWords = [['Sangat menarik', 6], ['Menarik', 5], ['Agak menarik', 4], ['Biasa saja', 3], ['Agak tidak menarik', 2], ['Tidak menarik', 1], ['Tidak sama sekali', 0]];
  const kohWords = [['Sangat koheren', 6], ['Koheren', 5], ['Agak koheren', 4], ['Biasa saja', 3], ['Agak tidak koheren', 2], ['Tidak koheren', 1], ['Tidak sama sekali', 0]];
  const lenWords = [['Sangat lengkap', 6], ['Lengkap', 5], ['Agak lengkap', 4], ['Biasa saja', 3], ['Agak tidak lengkap', 2], ['Tidak lengkap', 1], ['Tidak sama sekali', 0]];
  const [menarik, setMenarik] = React.useState({
    val: null,
    state: false
  });
  const [koheren, setKoheren] = React.useState({
    val: null,
    state: false
  });
  const [lengkap, setLengkap] = React.useState({
    val: null,
    state: false
  });
  const [valid, setValid] = React.useState(false);
  const [redirectDua, setRedirectDua] = React.useState(false);

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])

  const handleOnChange = (event) => {
    const val = event.target.value;
    if (event.target.name === 'menarik') {
      setMenarik({ val: val, state: true });
      if (koheren.state && lengkap.state) {
        setValid(true);
      }
    } else if (event.target.name === 'koheren') {
      setKoheren({ val: val, state: true });
      if (menarik.state && lengkap.state) {
        setValid(true);
      }
    } else if (event.target.name === 'lengkap') {
      setLengkap({ val: val, state: true });
      if (menarik.state && koheren.state) {
        setValid(true);
      }
    }
  };

  const handleSubmit = (m, k, l) => {
    dispatch(penil1(m, k, l));
    setRedirectDua(true);
  }

  const renderRedirectDua = () => {
    if (redirectDua === true) {
      return <Redirect to='/penilaian-2' />
    }
  }

  return (
    <Fragment>
      {renderRedirectDua()}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='justify' className={classes.content}>
              <b>Seberapa menarik tampilan berita tersebut?</b>
            </Typography>
            <Box display={{ xs: 'block', sm: 'none' }}>
              <RadioGroup aria-label="menarik" name="menarik" className={classes.mini} value={menarik.val} onChange={handleOnChange} >
                {menWords.map((elm) => {
                  return(
                    <FormControlLabel value={elm[1]} control={<Radio color="primary" checked={menarik.val == elm[1]} />} label={elm[0]} />
                  )
                })}
              </RadioGroup>
            </Box>
            <Box display={{ xs: 'none', sm: 'block' }}>
              <TableContainer elevation={0} className={classes.table} component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell style={{ color: '#FFFFFF' }}>Tidak sama sekali</TableCell>
                      {nilai.map((el) =>
                        <TableCell align="center">{el}</TableCell>
                      )}
                      <TableCell style={{ color: '#FFFFFF' }}>Sangat lengkap</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow key='questionOne'>
                      <TableCell component="th" scope="row">
                        Tidak sama sekali
                      </TableCell>
                      {nilai.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={menarik.val == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="menarik"
                          />
                        </TableCell>
                      )}
                      <TableCell component="th" scope="row">
                        Sangat menarik
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            <Typography align='justify' className={classes.content}>
              <b>Seberapa koheren struktur berita tersebut?</b>
            </Typography>
            <Box display={{ xs: 'block', sm: 'none' }}>
              <RadioGroup aria-label="koheren" name="koheren" className={classes.mini} value={koheren.val} onChange={handleOnChange} >
                {kohWords.map((elm) => {
                  return (
                    <FormControlLabel value={elm[1]} control={<Radio color="primary" checked={koheren.val == elm[1]} />} label={elm[0]} />
                  )
                })}
              </RadioGroup>
            </Box>
            <Box display={{ xs: 'none', sm: 'block' }}>
              <TableContainer elevation={0} className={classes.table} component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell style={{ color: '#FFFFFF' }}>Tidak sama sekali</TableCell>
                      {nilai.map((el) =>
                        <TableCell align="center">{el}</TableCell>
                      )}
                      <TableCell style={{ color: '#FFFFFF' }}>Sangat Koheren</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow key='questionOne'>
                      <TableCell component="th" scope="row">
                        Tidak sama sekali
                      </TableCell>
                      {nilai.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={koheren.val == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="koheren"
                          />
                        </TableCell>
                      )}
                      <TableCell component="th" scope="row">
                        Sangat koheren 
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            <Typography align='justify' className={classes.content}>
              <b>Apakah komponen dalam berita (judul, gambar, penulis) telah lengkap?</b>
            </Typography>
            <Box display={{ xs: 'block', sm: 'none' }}>
              <RadioGroup aria-label="lengkap" name="lengkap" className={classes.mini} value={lengkap.val} onChange={handleOnChange} >
                {lenWords.map((elm) => {
                  return (
                    <FormControlLabel value={elm[1]} control={<Radio color="primary" checked={lengkap.val == elm[1]} />} label={elm[0]} />
                  )
                })}
              </RadioGroup>
            </Box>
            <Box display={{ xs: 'none', sm: 'block' }}>
              <TableContainer elevation={0} className={classes.table} component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell style={{ color: '#FFFFFF' }}>Tidak sama sekali</TableCell>
                      {nilai.map((el) =>
                        <TableCell align="center">{el}</TableCell>
                      )}
                      <TableCell style={{ color: '#FFFFFF' }}>Sangat lengkap</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow key='questionOne'>
                      <TableCell component="th" scope="row">
                        Tidak sama sekali
                      </TableCell>
                      {nilai.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={lengkap.val == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="lengkap"
                          />
                        </TableCell>
                      )}
                      <TableCell component="th" scope="row">
                        Sangat lengkap
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            <div className={classes.right}>
              <Button disabled={!valid} variant="contained" size="medium" onClick={() => {handleSubmit(menarik.val, koheren.val, lengkap.val)}} className={classes.button}>
                <b>Lanjut</b>
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Penilaian1