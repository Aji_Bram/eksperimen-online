import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import { Button, Typography, Paper } from '@material-ui/core';
import { browserHistory } from 'react-router';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '3% 32% 3% 18%',
    [theme.breakpoints.down('md')]: {
      padding: '3% 22% 3% 12%',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '3% 20% 3% 6%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  judul: {
    marginBottom: '20px',
    [theme.breakpoints.down('xs')]: {
      fontSize: '9vw',
    },
  },
  content: {
    marginBottom: '20px'
  },
  button: {
    textAlign: 'left',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  noButton: {
    color: '#BE2E2E'
  }
}))

const LandingPage = () => {
  const classes = styles();

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])
  
  return (
    <Fragment>
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography variant='h3' className={classes.judul}><b>Penelitian Psikologi Sosial</b></Typography>
            <Typography align='justify' className={classes.content}>Salam jumpa,<br/>
              Saya Nesya Adira, mahasiswa Magister Psikologi Sosial Universitas Indonesia yang 
              sedang melakukan penelitian dasar tentang persepsi individu mengenai informasi politik 
              dan ekonomi Indonesia dan internasional yang diberitakan di media massa. Lebih lanjut 
              penelitian juga mengevaluasi sikap individu terhadap kebijakan ekonomi dan interaksi 
              individu dengan kelompok dalam hubungan internasional.
            </Typography>
            <Typography align='justify' className={classes.content}><b>Partisipasi</b><br/>
              Saya meminta kesediaan Anda secara sukarela dan dalam keadaan sadar mengisi kuesioner yang 
              tersedia dalam rangka penelitian saya. Selama pengisian kuesioner, Anda diperkenankan 
              mengundurkan diri dan tidak terdapat konsekuensi apapun. Namun demikian, saya sangat 
              berharap Anda bersedia untuk mengisi kuesioner ini sampai selesai.
            </Typography>
            <Typography align='justify' className={classes.content}><b>Apa yang diharapkan dari Anda</b><br />
              Kuesioner terdiri dari beberapa bagian. Pada bagian awal, Anda diminta untuk mengevaluasi 
              artikel yang berisi informasi politik dan ekonomi dan di bagian selanjutnya, Anda diminta 
              untuk merespon beberapa pernyataan yang sesuai dengan keadaan diri Anda. Pengisian kuesioner 
              akan memakan waktu Anda sekitar 10 menit. Sebelum mengisi kuesioner, Anda diharapkan untuk 
              membaca dengan benar instruksi pengisian kuesioner dan mengisi di kolom yang telah disediakan. 
              Semua jawaban yang Anda berikan <u><b>tidak dinilai benar atau salah.</b> Mohon Anda memberi jawaban 
              <b> sesuai dengan keadaan Anda sesungguhnya.</b></u>
            </Typography>
            <Typography align='justify' className={classes.content}><b>Kerahasiaan</b><br />
              Data pribadi Anda akan dijamin kerahasiaannya dan data kuesioner hanya akan digunakan untuk kepentingan 
              akademik. Setelah periode pengambilan data selesai, nama Anda akan diubah menjadi kode sehingga 
              peneliti tidak mengetahui identitas Anda secara individu. Semua data yang berkaitan dengan nama 
              akan dimusnahkan setelah penelitian ini selesai.
            </Typography>
            <Typography align='justify' className={classes.content}><b>Kelaikan Etik dan Kontak</b><br />
              Data pribadi Anda akan dijamin kerahasiaannya dan data kuesioner hanya akan digunakan untuk kepentingan
              akademik. Setelah periode pengambilan data selesai, nama Anda akan diubah menjadi kode sehingga
              peneliti tidak mengetahui identitas Anda secara individu. Semua data yang berkaitan dengan nama
              akan dimusnahkan setelah penelitian ini selesai.
            </Typography>
            <Typography align='justify' className={classes.content}>
              Adapun jika Anda ingin berbicara dengan petugas universitas yang dilibatkan dalam penelitian ini, 
              Anda juga dapat menghubungi kantor etika penelitian di Fakultas Psikologi, Universitas Indonesia 
              melalui surel: fpsi@ui.ac.id
            </Typography>
            <Typography align='justify' className={classes.content}><b>Pernyataan Persetujuan Ikut Serta dalam Penelitian</b><br />
              Saya telah membaca dan memahami tujuan dan teknis pengisian kuesioner ini serta hak-hak saya. 
              Saya <b>SETUJU/TIDAK SETUJU</b> berpartisipasi dalam penelitian ini.
            </Typography>
            <div className={classes.button}>
              <Link to="/instruction" style={{ textDecoration:'none',color:'black' }}><Button color="primary"><b>SETUJU</b></Button></Link>
              <Link to="/thanks-0" style={{ textDecoration:'none',color:'black' }}><Button className={classes.noButton}><b>TIDAK SETUJU</b></Button></Link>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default LandingPage