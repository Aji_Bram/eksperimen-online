import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'
import { Button, Typography, Paper } from '@material-ui/core';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '3% 32% 3% 18%',
    [theme.breakpoints.down('md')]: {
      padding: '3% 22% 3% 12%',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '3% 20% 3% 6%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    marginBottom: '20px'
  },
  button: {
    textAlign: 'left',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  noButton: {
    color: '#BE2E2E'
  }
}))

const LandingPage = () => {
  const classes = styles();

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])
  
  return (
    <Fragment>
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography variant='h3' align='center' className={classes.content}><b>PENUTUP</b></Typography>
            <Typography align='justify' className={classes.content}>
              Terima kasih telah berpartisipasi dalam penelitian ini.
            </Typography>
            <Typography align='justify' className={classes.content}>
              Penelitian ini dilaksanakan sebagai salah satu syarat menyelesaikan tugas akhir Magister Psikologi 
              Sosial. Terlebih dahulu peneliti memohon maaf karena atas dasar keperluan melindungi hasil penelitian 
              agar muncul secara alamiah dan sesuai dengan keadaan sebenarnya, peneliti menyamarkan tujuan sebenarnya 
              di bagian awal penelitian. Tujuan sebenarnya penelitian adalah untuk mengetahui apakah paparan berita 
              terkait isu investasi asing yang kontennya telah dimodifikasi mempengaruhi pembentukan sikap individu 
              terhadap dukungan untuk kebijakan investasi asing di Indonesia.
            </Typography>
            <Typography align='justify' className={classes.content}>
              Gagasan utamanya adalah meskipun investasi asing adalah kegiatan ekonomi yang umumnya dinilai berdasarkan 
              keuntungan dan kerugian ekonominya, terkadang dukungan terhadap kebijakan ini juga dipengaruhi oleh 
              faktor nilai dan budaya yang melekat pada pihak investor. Penelitian ini diharapkan dapat menambah 
              kontribusi bagi riset komunikasi politik tentang pengaruh informasi politik di media terhadap pembentukan 
              persepsi dan bagaimana hal ini dapat mempengaruhi individu menentukan dukungan terhadap suatu kebijakan pemerintah.
            </Typography>
            <Typography align='justify' className={classes.content}>
              Konten berita seluruhnya telah dimodifikasi untuk keperluan penelitian sehingga saya mengharapkan kesediaan 
              Anda untuk tidak menyebarluaskan, mempercayai dan menjadikan konten ini sebagai dasar untuk kepentingan apapun. 
              Selain itu, peneliti juga meminta kesediaan Anda untuk tidak menyebarluaskan tujuan sebenarnya penelitian kepada 
              calon partisipan yang belum menyelesaikan kuesioner.
            </Typography>
            <Typography align='justify' className={classes.content}>
              Jika Anda memiliki pertanyaan, kritik, saran atau ketertarikan khusus terhadap penelitian ini, Anda bisa langsung 
              menghubungi peneliti melalui surel: nesya.adira@ui.ac.id
            </Typography>
            <Typography align='justify' className={classes.content}>
              Sebagai bentuk apresiasi kepada partisipan, peneliti akan memberikan tanda terima kasih berupa tambahan saldo dalam 
              aplikasi uang elektronik dengan cara diundi. Apakah Anda berminat mengikuti undian tersebut?
            </Typography>
            <div className={classes.button}>
              <Link to="/lottery" style={{ textDecoration:'none', color:'black' }}><Button color="primary"><b>BERMINAT</b></Button></Link>
              <Link to="/thanks-1" style={{ textDecoration:'none', color:'black' }}><Button className={classes.noButton}><b>TIDAK BERMINAT</b></Button></Link>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default LandingPage