import React, { Fragment, useEffect, Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect, Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
// import { gapi } from 'gapi-script';
import { Button, Box, Typography, Paper, FormControl, InputLabel, MenuItem, Select, FormControlLabel, Grid, TextField, Table, TableContainer, TableBody, TableRow, TableCell } from '@material-ui/core';
import SendIcon from '@material-ui/icons/Send';

const SPREADSHEET_ID = '1zCoYZU2YcEGNGdVJ_K1ydWZykhxf_l2sq1-otPB2PXU';
const CLIENT_ID = '97045371253-u715lucbrc4hmpbce8m6iae05pde4hf2.apps.googleusercontent.com';
const API_KEY = 'AIzaSyDPosIKLjCRQLx2qcfWzcICEHAXB93S_gA';
const SCOPE = 'https://www.googleapis.com/auth/spreadsheets';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '3% 30% 3% 18%',
    [theme.breakpoints.down('md')]: {
      padding: '3% 22% 3% 8%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    marginLeft: '2%',
    width: '100%',
    marginBottom: '20px',
  },
  contentz: {
    marginLeft: '2%',
    width: '100%',
    marginBottom: '5px',
  },
  buttonChoose: {
    padding: "5px 33px",
  },
  button: {
    backgroundColor: theme.palette.primary.main,
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#090a3a',
      color: '#ebebeb',
    },
  },
  right: {
    paddingRight: '3%',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  left: {
    paddingLeft: '2%',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  form: {
    margin: '0 4% 2% 2%',
    width: "95%",
  },
  table: {
    marginBottom: '15px'
  },
  buttonChoosen: {
    padding: "5px 35px",
    margin: "5px 0"
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  other: {
    width: "40%",
    [theme.breakpoints.down('sm')]: {
      width: '80%',
    },
    paddingLeft: '6%',
    marginBottom: '3%',
  },
}))

const Information = () => {
  const classes = styles();
  const state = useSelector(state => state);
  const param = [6, 5, 4, 3, 2, 1, 0];
  const paramZ = [0, 1, 2, 3, 4, 5, 6];
  const sexist = ["Perempuan", "Laki-laki", "Lainnya"]
  const religy = ["Islam", "Kristen", "Katolik", "Hindu", "Budha", "Konghucu", "Lainnya"]
  const etnis = ["Jawa", "Sunda", "Batak", "Bugis", "Dayak", "Asmat", "Lainnya"]
  const edu = ["SD", "SMP", "SMA", "S1", "S2", "S3", "D1", "D2", "D3", "D4", "Lainnya"]
  const [values, setValues] = React.useState({
    names: "",
    age: "",
    agama: "",
    suku: "",
    sex: "",
    addr: "",
    education: "",
    job: "",
    otherSuku: "",
    sukuFix: "",
    sering: null,
    valid: false
  });
  const [redirect, setRedirect] = React.useState(false);

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])

  const handleOnChange = ( opt ) => event => {
    const { value } = event.target

    if (opt === 'names') {
      if (values.age !== '' && values.agama !== '' && values.suku !== '' && values.sex !== '' && values.addr !== '' && values.edu !== '' && values.job !== '' && values.sering !== null && value !== '') {
        if (values.suku === 'Lainnya' && values.otherSuku !== '') {
          setValues(prev => ({ ...prev, names: value, valid: true }))
        } else if (values.suku === 'Lainnya' && values.otherSuku === '') {
          setValues(prev => ({ ...prev, names: value, valid: false }))
        } else {
          setValues(prev => ({ ...prev, names: value, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, names: value, valid: false }));
      }
    } else if (opt === 'age') {
      var agess = /^[0-9\b]+$/;
      if (agess.test(value) || value === '') {
        if (values.names !== '' && values.agama !== '' && values.suku !== '' && values.sex !== '' && values.addr !== '' && values.edu !== '' && values.job !== '' && values.sering !== null && value !== '') {
          if (values.suku === 'Lainnya' && values.otherSuku !== '') {
            setValues(prev => ({ ...prev, age: value, valid: true }))
          } else if (values.suku === 'Lainnya' && values.otherSuku === '') {
            setValues(prev => ({ ...prev, age: value, valid: false }))
          } else {
            setValues(prev => ({ ...prev, age: value, valid: true }));
          }
        } else {
          setValues(prev => ({ ...prev, age: value, valid: false }));
        }
      }
    } else if (opt === 'agama') {
      if (values.age !== '' && values.names !== '' && values.suku !== '' && values.sex !== '' && values.addr !== '' && values.edu !== '' && values.job !== '' && values.sering !== null && value !== '') {
        if (values.suku === 'Lainnya' && values.otherSuku !== '') {
          setValues(prev => ({ ...prev, agama: value, valid: true }))
        } else if (values.suku === 'Lainnya' && values.otherSuku === '') {
          setValues(prev => ({ ...prev, agama: value, valid: false }))
        } else {
          setValues(prev => ({ ...prev, agama: value, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, agama: value, valid: false }));
      }
    } else if (opt === 'suku') {
      if (values.age !== '' && values.agama !== '' && values.names !== '' && values.sex !== '' && values.addr !== '' && values.edu !== '' && values.job !== '' && values.sering !== null && value !== '') {
        if (value === 'Lainnya' && values.otherSuku === '') {
          setValues(prev => ({ ...prev, suku: value, sukuFix: values.otherSuku, valid: false }))
        } else if (value === 'Lainnya' && values.otherSuku === '') {
          setValues(prev => ({ ...prev, suku: value, sukuFix: values.otherSuku, valid: false }))
        } else {
          setValues(prev => ({ ...prev, suku: value, sukuFix: value, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, suku: value, sukuFix: value, valid: false }));
      }
    } else if (opt === 'sex') {
      if (values.age !== '' && values.agama !== '' && values.suku !== '' && values.names !== '' && values.addr !== '' && values.edu !== '' && values.job !== '' && values.sering !== null && value !== '') {
        if (values.suku === 'Lainnya' && values.otherSuku !== '') {
          setValues(prev => ({ ...prev, sex: value, valid: true }))
        } else if (values.suku === 'Lainnya' && values.otherSuku === '') {
          setValues(prev => ({ ...prev, sex: value, valid: false }))
        } else {
          setValues(prev => ({ ...prev, sex: value, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, sex: value, valid: false }));
      }
    } else if (opt === 'addr') {
      if (values.age !== '' && values.agama !== '' && values.suku !== '' && values.sex !== '' && values.names !== '' && values.edu !== '' && values.job !== '' && values.sering !== null && value !== '') {
        if (values.suku === 'Lainnya' && values.otherSuku !== '') {
          setValues(prev => ({ ...prev, addr: value, valid: true }))
        } else if (values.suku === 'Lainnya' && values.otherSuku === '') {
          setValues(prev => ({ ...prev, addr: value, valid: false }))
        } else {
          setValues(prev => ({ ...prev, addr: value, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, addr: value, valid: false }));
      }
    } else if (opt === 'education') {
      if (values.age !== '' && values.agama !== '' && values.suku !== '' && values.sex !== '' && values.addr !== '' && values.names !== '' && values.job !== '' && values.sering !== null && value !== '') {
        if (values.suku === 'Lainnya' && values.otherSuku !== '') {
          setValues(prev => ({ ...prev, education: value, valid: true }))
        } else if (values.suku === 'Lainnya' && values.otherSuku === '') {
          setValues(prev => ({ ...prev, education: value, valid: false }))
        } else {
          setValues(prev => ({ ...prev, education: value, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, education: value, valid: false }));
      }
    } else if (opt === 'job') {
      if (values.age !== '' && values.agama !== '' && values.suku !== '' && values.sex !== '' && values.addr !== '' && values.edu !== '' && values.names !== '' && values.sering !== null && value !== '') {
        if (values.suku === 'Lainnya' && values.otherSuku !== '') {
          setValues(prev => ({ ...prev, job: value, valid: true }))
        } else if (values.suku === 'Lainnya' && values.otherSuku === '') {
          setValues(prev => ({ ...prev, job: value, valid: false }))
        } else {
          setValues(prev => ({ ...prev, job: value, valid: true }));
        }
      } else {
        setValues(prev => ({ ...prev, job: value, valid: false }));
      }
    } else if (opt === 'other') {
      if (values.age !== '' && values.agama !== '' && values.suku === 'Lainnya' && values.sex !== '' && values.addr !== '' && values.edu !== '' && values.names !== '' && values.sering !== null && values.job !== null && value !== '') {
        setValues(prev => ({ ...prev, otherSuku: value, sukuFix: value, valid: true }))
      } else {
        setValues(prev => ({ ...prev, otherSuku: value, sukuFix: value, valid: false }));
      }
    }
  };

  const handleChange = (opt, param) => {
    if (opt === 'sering') {
      if (values.age !== '' && values.agama !== '' && values.suku !== '' && values.sex !== '' && values.addr !== '' && values.edu !== '' && values.job !== '' && values.names !== '') {
        setValues(prev => ({ ...prev, sering: param , valid: true }))
      } else {
        setValues(prev => ({ ...prev, sering: param }));
      }
    }
  };

  const sendData = () => {
    setRedirect(true);
  };

  // const sendData = async e => {
  //   e.preventDefault();
  //   try {
  //     const response = await fetch(
  //       "https://v1.nocodeapi.com/ferrari689/google_sheets/wYUAERVmpKywqSvf?tabId=Eksperimen",
  //       {
  //         method: "post",
  //         body: JSON.stringify([[state.stim.stimulus, state.penil1.menarik, state.penil1.koheren, state.penil1.lengkap, state.penil2.one, state.penil2.two, state.penil2.three, state.penil2.four, state.penil2.five, state.penil2.six, state.penil2.seven, state.penil2.eight, state.penil2.nine, state.penil2.ten, state.penil2.eleven, state.penil2.twelve, state.penil2.thirteen, state.penil2.fourteen, state.penil2.fifteen, state.penil2.sixteen, state.penil2.seventeen, state.penil2.eighteen, state.penil2.nineteen, state.penil2.twenty, state.penil2.twentyone, state.penil2.twentytwo, state.penil3.sifatOne, state.penil3.sifatTwo, state.penil3.sifatThree, state.penil3.sifatFour, state.penil3.sifatFive, state.penil3.sifatSix, state.penil3.sifatSeven, state.penil3.sifatEight, state.penil3.sifatNine, state.penil3.sifatTen, state.penil4.sitCemas, state.penil4.sitRagu, state.penil4.sitKhawatir, state.penil4.sitCanggung, state.penil4.sitGugup, state.penil4.sitTerancam, state.penil5.fokusOne, state.penil5.fokusTwo, state.penil5.fokusCountry, values.names, values.age, values.agama, values.sukuFix, values.sex, values.addr, values.education, values.job, values.sering]]),
  //         headers: {
  //           "Content-Type": "application/json"
  //         }
  //       }
  //     );
  //     const json = await response.json();
  //     console.log("Success:", JSON.stringify(json));
  //     setRedirect(true);
  //   } catch (error) {
  //     console.error("Error:", error);
  //   }
  // };

  const renderRedirect = () => {
    if (redirect === true) {
      return <Redirect to='/closing' />
    }
  }

  return (
    <Fragment>
      {renderRedirect()}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='justify' className={classes.content}>
              Mohon Anda mengisi data diri responden berikut ini:
            </Typography>
            <TextField 
              id="outlined-basic" 
              label="Nama (boleh inisial)" 
              variant="outlined" 
              onChange={handleOnChange('names')}
              value={values.names}
              className={classes.form}
            />
            <TextField 
              id="outlined-basic" 
              label="Usia (angka)" 
              variant="outlined" 
              onChange={handleOnChange('age')}
              value={values.age}
              className={classes.form}
            />
            <FormControl variant="outlined" className={classes.form}>
              <InputLabel id="demo-simple-select-outlined-label">Agama</InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={values.agama}
                onChange={handleOnChange('agama')}
                label="Agama"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {religy.map((el) =>
                  <MenuItem value={el}>{el}</MenuItem>
                )}
              </Select>
            </FormControl>
            <FormControl variant="outlined" className={classes.form}>
              <InputLabel id="demo-simple-select-outlined-label">Suku/Etnis</InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={values.suku}
                onChange={handleOnChange('suku')}
                label="Suku/Etnis"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {etnis.map((el) =>
                  <MenuItem value={el}>{el}</MenuItem>
                )}
              </Select>
            </FormControl>
            <Box display={values.suku === 'Lainnya' ? "block" : "none"}>
              <TextField id="standard-basic" placeholder="Lainnya" className={classes.other} onChange={handleOnChange('other')} InputLabelProps={{ shrink: true, }} />
            </Box>
            <FormControl variant="outlined" className={classes.form}>
              <InputLabel id="demo-simple-select-outlined-label">Jenis Kelamin</InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={values.sex}
                onChange={handleOnChange('sex')}
                label="Jenis Kelamin"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {sexist.map((el) =>
                  <MenuItem value={el}>{el}</MenuItem>
                )}
              </Select>
            </FormControl>
            <TextField
              id="outlined-basic"
              label="Tempat tinggal"
              variant="outlined"
              onChange={handleOnChange('addr')}
              value={values.addr}
              className={classes.form}
            />
            <FormControl variant="outlined" className={classes.form}>
              <InputLabel id="demo-simple-select-outlined-label">Pendidikan Terakhir</InputLabel>
              <Select
                labelId="demo-simple-select-outlined-label"
                id="demo-simple-select-outlined"
                value={values.education}
                onChange={handleOnChange('education')}
                label="Pendidikan Terakhir"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {edu.map((el) =>
                  <MenuItem value={el}>{el}</MenuItem>
                )}
              </Select>
            </FormControl>
            <TextField
              id="outlined-basic"
              label="Pekerjaan"
              variant="outlined"
              onChange={handleOnChange('job')}
              value={values.job}
              className={classes.form}
            />
            
            <Typography align='justify' className={classes.contentz} style={{ marginTop: "5px" }}>
              <b>Seberapa sering Anda mengikuti berita politik dan membaca berita daring (di media online) ?</b>
            </Typography>
            <Box display={{ xs: 'block', sm: 'block', md: 'none' }}>
              <Grid container>
                <Grid item xs={12} sm={12} className={classes.center} style={{ marginTop: '5px' }}><Typography>Sering</Typography></Grid>
                {param.map((elm) => {
                  return(
                    <Grid item xs={12} sm={12} className={classes.center}>
                      <Button variant={values.sering === elm ? "contained" : "outlined"} value={elm} name='sering' size="large" color="primary" onClick={() => handleChange('sering', elm)} className={classes.buttonChoosen}>
                        {elm}
                      </Button>
                    </Grid>
                  )
                })}
                <Grid item xs={12} sm={12} className={classes.center} style={{ marginBottom: '10px' }}><Typography>Tidak pernah</Typography></Grid>
              </Grid>
            </Box>
            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
              <TableContainer elevation={0} className={classes.table} component={Paper}>
                <Table aria-label="simple table">
                  <TableBody>
                    <TableRow key='questionOne'>
                      {paramZ.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Button variant={values.sering === el ? "contained" : "outlined"} value={el} name='sering' size="large" color="primary" onClick={() => handleChange('sering', el)} className={classes.buttonChoose}>
                            {el}
                          </Button>
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionTwo'>
                      <TableCell component="th" scope="row" align='center' style={{ padding: "0px 16px" }}>
                        Tidak pernah
                      </TableCell>
                      <TableCell component="th" scope="row" colSpan={5} align='center' style={{ padding: "0px 16px" }}></TableCell>
                      <TableCell component="th" scope="row" align='center' style={{ padding: "0px 16px" }}>
                        Sering
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            
            <div className={classes.right}>
              <Button disabled={!values.valid} variant="contained" size="medium" onClick={sendData} className={classes.button}>
                <b>Submit </b><SendIcon fontSize='small' style={{ marginLeft: "6px" }}/>
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Information