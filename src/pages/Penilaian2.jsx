import React, { Fragment,useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom'
import { Button, Box, Typography, Paper, Radio, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, RadioGroup, FormControlLabel } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useDispatch } from 'react-redux';
import { penil2 } from '../redux/actions/DataAction';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '3% 29% 2% 17%',
    [theme.breakpoints.down('sm')]: {
      padding: '3% 22% 2% 8%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    marginLeft: '2%',
    width: '100%',
    marginBottom: '20px',
  },
  button: {
    marginTop: '4%',
    backgroundColor: theme.palette.primary.main,
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#090a3a',
      color: '#ebebeb',
    },
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  mini: {
    paddingLeft: '4%',
  },
}))

const Penilaian2 = () => {
  const classes = styles();
  const dispatch = useDispatch();
  const label = ["STS", "TS", "ATS", "AS", "S", "SS"];
  const labelLong = [["Sangat Setuju", 5], ["Setuju", 4], ["Agak Setuju", 3], ["Agak Tidak Setuju", 2], ["Tidak Setuju", 1], ["Sangat Tidak Setuju", 0]];
  const quest = [["Kedatangan investor asing dapat melunturkan budaya Indonesia", "one"], ["Kedatangan investor asing dapat menambah wawasan orang Indonesia mengenai budaya asing", "two"], ["Budaya Indonesia menjadi rusak karena datangnya investor asing", "three"], ["Pada dasarnya, nilai dan keyakinan terkait budaya kerja investor asing yang ada di Indonesia sama dengan orang Indonesia", "four"], ["Investor asing tidak harus mengikuti tradisi Indonesia", "five"], ["Nilai - nilai dan keyakinan terkait hubungan keluarga dan pergaulan investor asing tidak sesuai dengan nilai - nilai dan keyakinan orang Indonesia", "six"], ["Nilai dan kepercayaan yang dipegang investor asing mengenai isu moral tidak sesuai dengan keyakinan dan nilai orang - orang Indonesia", "seven"], ["Keyakinan dan nilai - nilai agama yang dipegang investor asing tidak sesuai dengan keyakinan dan nilai - nilai agama orang Indonesia", "eight"], ["Identitas bangsa menjadi terancam karena banyaknya investor asing yang masuk ke Indonesia", "nine"], ["Keberadaan investor asing di Indonesia membawa pengaruh buruk bagi masyarakat Indonesia", "ten"], ["Investor asing menerima keuntungan yang diperuntukkan bagi masyarakat Indonesia", "eleven"], ["Investor asing mengambil sumber daya Indonesia lebih banyak daripada yang mereka berikan untuk Indonesia", "twelve"], ["Investor asing merugikan perekonomian negara", "thirteen"], ["Keberadaan investor asing menurunkan tingkat pengangguran di Indonesia", "fourteen"], ["Keberadaan investor asing membuat peluang pasar menjadi lebih sempit", "fifteen"], ["Investor asing membuat produk lokal kalah saing dengan produk asing", "sixteen"], ["Keberadaan investor asing mempermudah masuknya tenaga kerja asing yang merebut lapangan pekerjaan tenaga kerja Indonesia", "seventeen"], ["Saya kurang mendukung kebijakan untuk menarik investasi asing masuk ke Indonesia", "eighteen"], ["Tingginya jumlah investasi asing yang ada di Indonesia memprihatinkan", "nineteen"], ["Investasi asing di Indonesia seharusnya dilarang", "twenty"], ["Sumber daya alam kita seharusnya dijauhkan dari campur tangan investasi asing", "twentyone"], ["Indonesia tidak membutuhkan investasi asing", "twentytwo"]];
  const stats = [0, 1, 2, 3, 4, 5];
  const [value, setValue] = React.useState({
    one: 99, two: 99, three: 99, four: 99, five: 99, six: 99, seven: 99, eight: 99, nine: 99, ten: 99, eleven: 99,
    twelve: 99, thirteen: 99, fourteen: 99, fifteen: 99, sixteen: 99, seventeen: 99, eighteen: 99, nineteen: 99, twenty: 99, 
    twentyone: 99, twentytwo: 99, valid: false,
  });
  const [redirect, setRedirect] = React.useState(false);

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])

  const handleCheck = () => {
    if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.one !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.one !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.one !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.one !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.one !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.one !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.one !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.one !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.one !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.one !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.one !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.one !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.one !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.one !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.one !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.one !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.one !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.one !== 99 && value.twenty !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.one !== 99 && value.twentyone !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.one !== 99 && value.twentytwo !== 99) {
      return true;
    } else if (value.one !== 99 && value.two !== 99 && value.three !== 99 && value.four !== 99 && value.five !== 99 && value.six !== 99 && value.seven !== 99 && value.eight !== 99 && value.nine !== 99 && value.ten !== 99 && value.eleven !== 99 && value.twelve !== 99 && value.thirteen !== 99 && value.fourteen !== 99 && value.fifteen !== 99 && value.sixteen !== 99 && value.seventeen !== 99 && value.eighteen !== 99 && value.nineteen !== 99 && value.twenty !== 99 && value.twentyone !== 99) {
      return true;
    } else {
      return false;
    }
  }
  const handleOnChange = (event) => {
    const val = event.target.value;
    if (event.target.name === 'one') {
      setValue(prev => ({ ...prev, one: val }));
    } else if (event.target.name === 'two') {
      setValue(prev => ({ ...prev, two: val }));
    } else if (event.target.name === 'three') {
      setValue(prev => ({ ...prev, three: val }));
    } else if (event.target.name === 'four') {
      setValue(prev => ({ ...prev, four: val }));
    } else if (event.target.name === 'five') {
      setValue(prev => ({ ...prev, five: val }));
    } else if (event.target.name === 'six') {
      setValue(prev => ({ ...prev, six: val }));
    } else if (event.target.name === 'seven') {
      setValue(prev => ({ ...prev, seven: val }));
    } else if (event.target.name === 'eight') {
      setValue(prev => ({ ...prev, eight: val }));
    } else if (event.target.name === 'nine') {
      setValue(prev => ({ ...prev, nine: val }));
    } else if (event.target.name === 'ten') {
      setValue(prev => ({ ...prev, ten: val }));
    } else if (event.target.name === 'eleven') {
      setValue(prev => ({ ...prev, eleven: val }));
    } else if (event.target.name === 'twelve') {
       setValue(prev => ({ ...prev, twelve: val }));
    } else if (event.target.name === 'thirteen') {
      setValue(prev => ({ ...prev, thirteen: val }));
    } else if (event.target.name === 'fourteen') {
      setValue(prev => ({ ...prev, fourteen: val }));
    } else if (event.target.name === 'fifteen') {
      setValue(prev => ({ ...prev, fifteen: val }));
    } else if (event.target.name === 'sixteen') {
      setValue(prev => ({ ...prev, sixteen: val }));
    } else if (event.target.name === 'seventeen') {
      setValue(prev => ({ ...prev, seventeen: val }));
    } else if (event.target.name === 'eighteen') {
      setValue(prev => ({ ...prev, eighteen: val }));
    } else if (event.target.name === 'nineteen') {
      setValue(prev => ({ ...prev, nineteen: val }));
    } else if (event.target.name === 'twenty') {
      setValue(prev => ({ ...prev, twenty: val }));
    } else if (event.target.name === 'twentyone') {
      setValue(prev => ({ ...prev, twentyone: val }));
    } else if (event.target.name === 'twentytwo') {
      setValue(prev => ({ ...prev, twentytwo: val }));
    }

    if (handleCheck()) {
      setValue(prev => ({ ...prev, valid: true }));
    }
  };

  const handleSubmit = (a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v) => {
    dispatch(penil2(a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v));
    setRedirect(true);
  }

  const renderRedirect = () => {
    if (redirect === true) {
      return <Redirect to='/penilaian-3' />
    }
  }

  return (
    <Fragment>
      {renderRedirect()}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='justify' className={classes.content}>
              Pada bagian ini, Anda diminta untuk menentukan persetujuan Anda terhadap pernyataan-pernyataan yang 
              berkaitan dengan isu yang tengah berkembang di masyarakat. Pilihlah salah satu dari alternatif pilihan 
              yang sesuai dengan diri Anda. Tidak ada jawaban benar atau salah. Keterangan pilihan jawaban:
            </Typography>
            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
              <Typography align='justify' className={classes.content}>
                <b>STS</b>&nbsp;&nbsp;&nbsp;: Jika Anda <b>Sangat Tidak Setuju</b> dengan pernyataan tersebut
              </Typography>
              <Typography align='justify' className={classes.content}>
                <b>TS</b>&nbsp;&nbsp;&nbsp;: Jika Anda <b>Tidak Setuju</b> dengan pernyataan tersebut
              </Typography>
              <Typography align='justify' className={classes.content}>
                <b>ATS</b>&nbsp;&nbsp;&nbsp;: Jika Anda <b>Agak Tidak Setuju</b> dengan pernyataan tersebut
              </Typography>
              <Typography align='justify' className={classes.content}>
                <b>AS</b>&nbsp;&nbsp;&nbsp;: Jika Anda <b>Agak Setuju</b> dengan pernyataan tersebut
              </Typography>
              <Typography align='justify' className={classes.content}>
                <b>S</b>&nbsp;&nbsp;&nbsp;: Jika Anda <b>Setuju</b> dengan pernyataan tersebut
              </Typography>
              <Typography align='justify' className={classes.content}>
                <b>SS</b>&nbsp;&nbsp;&nbsp;: Jika Anda <b>Sangat Setuju</b> dengan pernyataan tersebut
              </Typography>
            </Box>
            <Box display={{ xs: 'block', sm: 'block', md: 'none' }}>
              {quest.map((element) => {
                return (
                  <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                      <Typography className={classes.heading}>{element[0]}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <RadioGroup aria-label={element[1]} name={element[1]} className={classes.mini} value={element[1] === "one" ? value.one : element[1] === "two" ? value.two : element[1] === "three" ? value.three : element[1] === "four" ? value.four : element[1] === "five" ? value.five : element[1] === "six" ? value.six : element[1] === "seven" ? value.seven : element[1] === "eight" ? value.eight : element[1] === "nine" ? value.nine : element[1] === "ten" ? value.ten : element[1] === "eleven" ? value.eleven : element[1] === "twelve" ? value.twelve : element[1] === "thirteen" ? value.thirteen : element[1] === "fourteen" ? value.fourteen : element[1] === "fifteen" ? value.fifteen : element[1] === "sixteen" ? value.sixteen : element[1] === "seventeen" ? value.seventeen : element[1] === "eighteen" ? value.eighteen : element[1] === "nineteen" ? value.nineteen : element[1] === "twenty" ? value.twenty : element[1] === "twentyone" ? value.twentyone : value.twentytwo} onChange={handleOnChange}>
                        {labelLong.map((elm) => {
                          return (
                            <FormControlLabel value={elm[1]} control={<Radio color="primary" checked={element[1] === "one" ? value.one == elm[1] : element[1] === "two" ? value.two == elm[1] : element[1] === "three" ? value.three == elm[1] : element[1] === "four" ? value.four == elm[1] : element[1] === "five" ? value.five == elm[1] : element[1] === "six" ? value.six == elm[1] : element[1] === "seven" ? value.seven == elm[1] : element[1] === "eight" ? value.eight == elm[1] : element[1] === "nine" ? value.nine == elm[1] : element[1] === "ten" ? value.ten == elm[1] : element[1] === "eleven" ? value.eleven == elm[1] : element[1] === "twelve" ? value.twelve == elm[1] : element[1] === "thirteen" ? value.thirteen == elm[1] : element[1] === "fourteen" ? value.fourteen == elm[1] : element[1] === "fifteen" ? value.fifteen == elm[1] : element[1] === "sixteen" ? value.sixteen == elm[1] : element[1] === "seventeen" ? value.seventeen == elm[1] : element[1] === "eighteen" ? value.eighteen == elm[1] : element[1] === "nineteen" ? value.nineteen == elm[1] : element[1] === "twenty" ? value.twenty == elm[1] : element[1] === "twentyone" ? value.twentyone == elm[1] : value.twentytwo == elm[1]} />} label={elm[0]} />
                          )
                        })}
                      </RadioGroup>
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                )
              })}
            </Box>
            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
              <TableContainer elevation={2} component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell width='200px'></TableCell>
                      {label.map((el) => 
                        <TableCell align="center">{el}</TableCell>
                      )}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow key='questionOne'>
                      <TableCell component="th" scope="row">
                        Kedatangan investor asing dapat melunturkan budaya Indonesia
                      </TableCell>
                      {stats.map((el) => 
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.one == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="one"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionTwo'>
                      <TableCell component="th" scope="row">
                        Kedatangan investor asing dapat menambah wawasan orang Indonesia mengenai budaya asing
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.two == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="two"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionThree'>
                      <TableCell component="th" scope="row">
                        Budaya Indonesia menjadi rusak karena datangnya investor asing
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.three == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="three"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionFour'>
                      <TableCell component="th" scope="row">
                        Pada dasarnya, nilai dan keyakinan terkait budaya kerja investor asing yang ada di Indonesia sama dengan orang Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.four == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="four"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionfive'>
                      <TableCell component="th" scope="row">
                        Investor asing tidak harus mengikuti tradisi Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.five == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="five"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionsix'>
                      <TableCell component="th" scope="row">
                        Nilai-nilai dan keyakinan terkait hubungan keluarga dan pergaulan investor asing tidak sesuai dengan nilai-nilai dan keyakinan orang Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.six == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="six"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionseven'>
                      <TableCell component="th" scope="row">
                        Nilai dan kepercayaan yang dipegang investor asing mengenai isu moral tidak sesuai dengan keyakinan dan nilai orang-orang Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.seven == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="seven"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questioneight'>
                      <TableCell component="th" scope="row">
                        Keyakinan dan nilai-nilai agama yang dipegang investor asing tidak sesuai dengan keyakinan dan nilai-nilai agama orang Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.eight == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="eight"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionnine'>
                      <TableCell component="th" scope="row">
                        Identitas bangsa menjadi terancam karena banyaknya investor asing yang masuk ke Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.nine == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="nine"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionten'>
                      <TableCell component="th" scope="row">
                        Keberadaan investor asing di Indonesia membawa pengaruh buruk bagi masyarakat Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.ten == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="ten"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questioneleven'>
                      <TableCell component="th" scope="row">
                        Investor asing menerima keuntungan yang diperuntukkan bagi masyarakat Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.eleven == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="eleven"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questiontwelve'>
                      <TableCell component="th" scope="row">
                        Investor asing mengambil sumber daya Indonesia lebih banyak daripada yang mereka berikan untuk Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.twelve == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="twelve"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionthirteen'>
                      <TableCell component="th" scope="row">
                        Investor asing merugikan perekonomian negara
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.thirteen == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="thirteen"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionfourteen'>
                      <TableCell component="th" scope="row">
                        Keberadaan investor asing menurunkan tingkat pengangguran di Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.fourteen == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="fourteen"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionfifteen'>
                      <TableCell component="th" scope="row">
                        Keberadaan investor asing membuat peluang pasar menjadi lebih sempit
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.fifteen == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="fifteen"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionsixteen'>
                      <TableCell component="th" scope="row">
                        Investor asing membuat produk lokal kalah saing dengan produk asing
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.sixteen == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="sixteen"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionseventeen'>
                      <TableCell component="th" scope="row">
                        Keberadaan investor asing mempermudah masuknya tenaga kerja asing yang merebut lapangan pekerjaan tenaga kerja Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.seventeen == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="seventeen"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questioneighteen'>
                      <TableCell component="th" scope="row">
                        Saya kurang mendukung kebijakan untuk menarik investasi asing masuk ke Indonesia
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.eighteen == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="eighteen"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionnineteen'>
                      <TableCell component="th" scope="row">
                        Tingginya jumlah investasi asing yang ada di Indonesia memprihatinkan
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.nineteen == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="nineteen"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questiontwenty'>
                      <TableCell component="th" scope="row">
                        Investasi asing di Indonesia seharusnya dilarang
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.twenty == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="twenty"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questiontwentyone'>
                      <TableCell component="th" scope="row">
                        Sumber daya alam kita seharusnya dijauhkan dari campur tangan investasi asing
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.twentyone == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="twentyone"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questiontwentytwo'>
                      <TableCell component="th" scope="row">
                        Indonesia tidak membutuhkan investasi asing
                      </TableCell>
                      {stats.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.twentytwo == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="twentytwo"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            <div className={classes.right}>
              <Button disabled={!value.valid} variant="contained" size="medium" onClick={() => {
                handleSubmit(value.one, value.two, value.three, value.four, value.five, value.six, value.seven, value.eight, value.nine, value.ten, value.eleven, value.twelve, value.thirteen, value.fourteen, value.fifteen, value.sixteen, value.seventeen, value.eighteen, value.nineteen, value.twenty, value.twentyone, value.twentytwo)}}
                className={classes.button}>
                <b>Lanjut</b>
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Penilaian2