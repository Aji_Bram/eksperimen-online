import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom'
import { Button, Typography, Paper } from '@material-ui/core';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    backgroundSize: 'auto',
    width: '100%',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '12% 32% 3% 22%',
    [theme.breakpoints.down('md')]: {
      padding: '12% 22% 3% 12%',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '12% 20% 3% 6%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    marginLeft: '4%',
    width: '92%',
    marginBottom: '20px',
  },
  button: {
    marginRight: '4%',
    backgroundColor: '#008506',
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#005e04',
      color: '#ebebeb',
    },
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
}))

const Instruction = () => {
  const classes = styles();
  
  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])

  const [values,setValues] = React.useState({
    stim: 0,
    redirect: false
  })
  
  const random = () => {
    var list = [1, 2, 3, 4, 5, 6, 7]
    var min = 0;
    var max = list.length;
    var rand = min + (Math.random() * (max - min));
    rand = Math.floor(rand);
    setValues(prev => ({ ...prev, stim: list[rand], redirect: true }));
  }

  const renderRedirect = (param) => {
    if (values.redirect === true) {
      return <Redirect to={`/stimulus?id=${param}`} />
    }
  }

  return (
    <Fragment>
      {renderRedirect(values.stim)}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='justify' className={classes.content}>Pada bagian ini, Anda akan diarahkan ke salah satu laman 
              berita daring. Anda diminta untuk memperhatikan baik-baik tampilan laman artikel berita di sebuah media daring dan memberi penilaian 
              terhadap tampilan artikel pada media daring ini dengan rentang penilaian dari 0 (sangat buruk) hingga 6 (sangat baik).
            </Typography>
            <Typography align='justify' className={classes.content}>
              Kriteria penilaian antara lain adalah kemenarikan, struktur konten dan kelengkapan 
              komponen berita. Panduan selanjutnya akan muncul otomatis setelah Anda selesai mengevaluasi artikel berita.
            </Typography>
            <div className={classes.right}>
              <Button variant="contained" size="medium" className={classes.button} onClick={() => random()}>
                <b>MULAI</b>
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Instruction