import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom'
import { useDispatch } from 'react-redux';
import { Button, Typography, Paper, Grid } from '@material-ui/core';
import queryString from 'query-string';
import News from '../components/News';
import { stim } from '../redux/actions/DataAction';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    backgroundSize: 'auto',
    width: '100%',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '2% 32% 2% 22%',
    [theme.breakpoints.down('lg')]: {
      padding: '2% 32% 3% 22%',
    },
    [theme.breakpoints.down('md')]: {
      padding: '2% 22% 3% 12%',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '2% 20% 3% 6%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    marginLeft: '4%',
    width: '92%',
    marginBottom: '20px',
  },
  button: {
    marginRight: '4%',
    backgroundColor: theme.palette.primary.main,
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#090a3a',
      color: '#ebebeb',
    },
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  photo: {
    width: '350px',
    height: '200px',
    marginBottom: '30px'
  }
}))

const Stimulus = (props) => {
  const classes = styles();
  const dispatch = useDispatch();
  const [show, setShow] = React.useState(true);
  const [second, setSecond] = React.useState(30);
  const [stimulus, setStim] = React.useState(0);
  const [redirect, setRedirect] = React.useState(false);

  useEffect(() => {
    const param = queryString.parse(props.location.search);
    setStim(param.id);
  }, [props])

  const setCountDown = () => {
    var seconds = 30;
    var x = setInterval(() => {

      if (seconds > 0) {
        setSecond(seconds - 1);
        seconds = seconds - 1;
      } else {
        setShow(false);
        clearInterval(x);
      }
    }, 1000)
  }

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
    setCountDown();
  }, [])

  const handleNext = () => {
    dispatch(stim(stimulus));
    setRedirect(true);
  }

  const renderRedirect = () => {
    if (redirect === true) {
      return <Redirect to='/penilaian-1' />
    }
  }

  return (
    <Fragment>
      {renderRedirect()}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <News stimulus={stimulus} />
            <div className={classes.right}>
              { show ? 
                <div>
                  <Grid container>
                    <Grid item xs={9} md={9}>
                      <Typography style={{ marginTop: '6px' }}>{ second + ' detik lagi untuk dapat lanjut' }</Typography>
                    </Grid>
                    <Grid item xs={3} md={3}>
                      <Button disabled variant="contained" size="medium" className={classes.button}>
                        <b>Lanjut</b>
                      </Button>
                    </Grid>
                  </Grid>
                </div>
                :
                <Button variant="contained" size="medium" className={classes.button} onClick={() => {handleNext()}}>
                  <b>Lanjut</b>
                </Button>
              }
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Stimulus