import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom'
import { Button, Slider, Typography, Paper, Radio, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, ExpansionPanel, ExpansionPanelSummary, ExpansionPanelDetails, RadioGroup, FormControlLabel, Box } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { useDispatch } from 'react-redux';
import { penil3 } from '../redux/actions/DataAction';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '3% 22% 2% 10%',
    [theme.breakpoints.down('md')]: {
      padding: '3% 22% 2% 8%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    width: '100%',
    marginBottom: '20px',
  },
  button: {
    marginTop: '4%',
    backgroundColor: theme.palette.primary.main,
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#090a3a',
      color: '#ebebeb',
    },
  },
  right: {
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
}))

const Penilaian3 = () => {
  const classes = styles();
  const dispatch = useDispatch();
  const quest = [["Tidak Jujur", "one", "ones"], ["Bebal; keras kepala", "two", "twos"], ["Tidak Beradab", "three", "threes"], ["Bodoh", "four", "fours"], ["Pelit", "five", "fives"]];
  const sifat = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const persen = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  const [value, setValue] = React.useState({
    one: 0,
    two: 0,
    three: 0,
    four: 0,
    five: 0,
    ones: -10,
    twos: -10,
    threes: -10,
    fours: -10,
    fives: -10,
    valid: false,
  });
  const marks = [
    { value: 1, label: '1', },
    { value: 2, label: '2', },
    { value: 3, label: '3', },
    { value: 4, label: '4', },
    { value: 5, label: '5', },
    { value: 6, label: '6', },
    { value: 7, label: '7', },
    { value: 8, label: '8', },
    { value: 9, label: '9', },
    { value: 10, label: '10', },
  ];
  const marksOne = [
    { value: 0, label: '0', },
    { value: 10, label: '10', },
    { value: 20, label: '20', },
    { value: 30, label: '30', },
    { value: 40, label: '40', },
    { value: 50, label: '50', },
    { value: 60, label: '60', },
    { value: 70, label: '70', },
    { value: 80, label: '80', },
    { value: 90, label: '90', },
    { value: 100, label: '100', },
  ];
  const [redirect, setRedirect] = React.useState(false);

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])

  const handleOnChange = (event) => {
    const val = event.target.value;

    if (event.target.name === 'one') {
      if (value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, one: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, one: val }));
      }
    } else if (event.target.name === 'two') {
      if (value.one !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, two: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, two: val }));
      }
    } else if (event.target.name === 'three') {
      if (value.one !== 0 && value.two !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, three: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, three: val }));
      }
    } else if (event.target.name === 'four') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, four: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, four: val }));
      }
    } else if (event.target.name === 'five') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, five: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, five: val }));
      }
    }
    else if (event.target.name === 'ones') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, ones: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, ones: val }));
      }
    }
    else if (event.target.name === 'twos') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, twos: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, twos: val }));
      }
    }
    else if (event.target.name === 'threes') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, threes: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, threes: val }));
      }
    }
    else if (event.target.name === 'fours') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, fours: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, fours: val }));
      }
    }
    else if (event.target.name === 'fives') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10) {
        setValue(prev => ({ ...prev, fives: val, valid: true }));
      } else {
        setValue(prev => ({ ...prev, fives: val }));
      }
    }
  };

  const handleSliderChange = (name) => (event, newValue) => {
    
    if (name === 'one') {
      if (value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, one: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, one: newValue }));
      }
    } else if (name === 'two') {
      if (value.one !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, two: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, two: newValue }));
      }
    } else if (name === 'three') {
      if (value.one !== 0 && value.two !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, three: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, three: newValue }));
      }
    } else if (name === 'four') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, four: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, four: newValue }));
      }
    } else if (name === 'five') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, five: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, five: newValue }));
      }
    }
    else if (name === 'ones') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, ones: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, ones: newValue }));
      }
    }
    else if (name === 'twos') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.threes !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, twos: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, twos: newValue }));
      }
    }
    else if (name === 'threes') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.fours !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, threes: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, threes: newValue }));
      }
    }
    else if (name === 'fours') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fives !== -10) {
        setValue(prev => ({ ...prev, fours: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, fours: newValue }));
      }
    }
    else if (name === 'fives') {
      if (value.one !== 0 && value.two !== 0 && value.three !== 0 && value.four !== 0 && value.five !== 0 && value.ones !== -10 && value.twos !== -10 && value.threes !== -10 && value.fours !== -10) {
        setValue(prev => ({ ...prev, fives: newValue, valid: true }));
      } else {
        setValue(prev => ({ ...prev, fives: newValue }));
      }
    }
  };

  const handleSubmit = (a, b, c, d, e, f, g, h, i, j) => {
    dispatch(penil3(a, b, c, d, e, f, g, h, i, j));
    setRedirect(true);
  }

  const renderRedirect = () => {
    if (redirect === true) {
      return <Redirect to='/penilaian-4' />
    }
  }

  return (
    <Fragment>
      {renderRedirect()}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='justify' className={classes.content}>
              Pada bagian ini, Anda diminta untuk memberi nilai terhadap kata-kata sifat di bawah ini dari rentang 1-10 (1=sangat negatif; 10=sangat positif)
            </Typography>
            <Box display={{ xs: 'block', sm: 'block', md: 'none' }}>
              {quest.map((element) => {
                return (
                  <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                      <Typography className={classes.heading}>{element[0]}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <Slider
                        value={element[1] === "one" ? value.one : element[1] === "two" ? value.two : element[1] === "three" ? value.three : element[1] === "four" ? value.four : value.five}
                        min={0}
                        step={1}
                        max={10}
                        onChange={handleSliderChange(element[1])}
                        valueLabelDisplay="auto"
                        aria-labelledby="non-linear-slider"
                        marks={marks}
                      />
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                )
              })}
            </Box>
            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
              <TableContainer className={classes.table} component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell style={{color:'#FFFFFF'}}>.................................</TableCell>
                      {sifat.map((el) => 
                        <TableCell align="center">{el}</TableCell>
                      )}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow key='questionOne'>
                      <TableCell component="th" scope="row">
                        Tidak Jujur
                      </TableCell>
                      {sifat.map((el) => 
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.one == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="one"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionTwo'>
                      <TableCell component="th" scope="row">
                        Bebal; keras kepala
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.two == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="two"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionThree'>
                      <TableCell component="th" scope="row">
                        Tidak Beradab
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.three == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="three"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionFour'>
                      <TableCell component="th" scope="row">
                        Bodoh
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.four == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="four"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionFive'>
                      <TableCell component="th" scope="row">
                        Pelit
                      </TableCell>
                      {sifat.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.five == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="five"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            <Typography align='justify' className={classes.content} style={{ marginTop: '20px' }}>
              Selanjutnya, berikan penilaian terhadap seberapa besar kata-kata sifat ini merepresentasikan sifat yang dimiliki investor asing yang ada di Indonesia. Beri nilai dengan persentase dari rentang (0% = tidak sama sekali, hingga 100% = sangat mencerminkan)
            </Typography>
            <Box display={{ xs: 'block', sm: 'block', md: 'none' }}>
              {quest.map((element) => {
                return (
                  <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1a-content" id="panel1a-header">
                      <Typography className={classes.heading}>{element[0]}</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                      <Slider
                        value={element[2] === "ones" ? value.ones : element[2] === "twos" ? value.twos : element[2] === "threes" ? value.threes : element[2] === "fours" ? value.fours : value.fives}
                        min={-10}
                        step={10}
                        max={100}
                        onChange={handleSliderChange(element[2])}
                        aria-labelledby="non-linear-slider"
                        marks={marksOne}
                      />
                    </ExpansionPanelDetails>
                  </ExpansionPanel>
                )
              })}
            </Box>
            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
              <TableContainer className={classes.table} component={Paper}>
                <Table aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell style={{ color: '#FFFFFF' }}>.................................</TableCell>
                      {persen.map((el) =>
                        <TableCell align="center">{el}</TableCell>
                      )}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    <TableRow key='questionOnes'>
                      <TableCell component="th" scope="row">
                        Tidak Jujur
                      </TableCell>
                      {persen.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.ones == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="ones"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionTwos'>
                      <TableCell component="th" scope="row">
                        Bebal; keras kepala
                      </TableCell>
                      {persen.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.twos == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="twos"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionThrees'>
                      <TableCell component="th" scope="row">
                        Tidak Beradab
                      </TableCell>
                      {persen.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.threes == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="threes"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionFours'>
                      <TableCell component="th" scope="row">
                        Bodoh
                      </TableCell>
                      {persen.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.fours == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="fours"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionFives'>
                      <TableCell component="th" scope="row">
                        Pelit
                      </TableCell>
                      {persen.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Radio
                            checked={value.fives == el}
                            onChange={handleOnChange}
                            value={el}
                            color="primary"
                            name="fives"
                          />
                        </TableCell>
                      )}
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            <div className={classes.right}>
              <Button disabled={!value.valid} variant="contained" size="medium" onClick={() => {
                handleSubmit(value.one, value.two, value.three, value.four, value.five, value.ones, value.twos, value.threes, value.fours, value.fives)}}
                className={classes.button}>
                <b>Lanjut</b>
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Penilaian3