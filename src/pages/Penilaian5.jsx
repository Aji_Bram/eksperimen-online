import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom'
import { Button, Typography, Paper, FormGroup, FormControlLabel, Grid, Checkbox, Table, TableContainer, TableBody, TableRow, TableCell, Box } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { penil5 } from '../redux/actions/DataAction';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    width: '100vw',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '5% 29% 3% 17%',
    [theme.breakpoints.down('md')]: {
      padding: '5% 22% 2% 8%',
    },
    [theme.breakpoints.down('sm')]: {
      padding: '5% 20% 2% 5%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    marginLeft: '2%',
    width: '100%',
    marginBottom: '20px',
  },
  buttonChoose: {
    padding: "5px 35px",
  },
  buttonChoose: {
    padding: "5px 35px",
  },
  buttonChoosen: {
    padding: "5px 35px",
    margin: "5px 0"
  },
  button: {
    backgroundColor: theme.palette.primary.main,
    color: '#FFFFFF',
    '&:hover': {
      backgroundColor: '#090a3a',
      color: '#ebebeb',
    },
  },
  center: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  right: {
    paddingRight: '4%',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  left: {
    paddingLeft: '2%',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  table: {
    marginBottom: '15px'
  },
  space: {
    margin: 'px 0'
  },
}))

const Penilaian5 = () => {
  const classes = styles();
  const dispatch = useDispatch();
  const param = [6, 5, 4, 3, 2, 1, 0];
  const paramZ = [0, 1, 2, 3, 4, 5, 6];
  const negara = ["China", "Amerika Serikat", "Arab Saudi", "Jepang", "Tidak Jelaskan"];
  const [values, setValues] = React.useState({
    first: null,
    second: null,
    third: null,
    valid: false
  });
  const [redirect, setRedirect] = React.useState(false);

  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])

  const handleOnChange = (opt, param) => {
    if (opt === 'first') {
      if (values.second !== null && values.third !== null) {
        setValues(prev => ({ ...prev, first: param, valid: true }));
      } else {
        setValues(prev => ({ ...prev, first: param }));
      }
    } else if (opt === 'second') {
      if (values.first !== null && values.third !== null) {
        setValues(prev => ({ ...prev, second: param, valid: true }));
      } else {
        setValues(prev => ({ ...prev, second: param }));
      }
    } else if (opt === 'third') {
      if (values.second !== null && values.first !== null) {
        setValues(prev => ({ ...prev, third: param, valid: true }));
      } else {
        setValues(prev => ({ ...prev, third: param }));
      }
    }
  };

  const handleSubmit = (f, s, t) => {
    dispatch(penil5(f, s, t));
    setRedirect(true);
  }

  const renderRedirect = () => {
    if (redirect === true) {
      return <Redirect to='/information' />
    }
  }

  return (
    <Fragment>
      {renderRedirect()}
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='justify' className={classes.content}>
              <b>Seberapa fokus Anda mengerjakan kuesioner ini?</b>
            </Typography>
            <Box display={{ xs: 'block', sm: 'block', md: 'none' }}>
              <Grid container>
                <Grid item xs={12} sm={12} className={classes.center} style={{ marginTop: '5px' }}><Typography>Sangat fokus</Typography></Grid>
                {param.map((elm) => {
                  return(
                    <Grid item xs={12} sm={12} className={classes.center}>
                      <Button variant={values.first === elm ? "contained" : "outlined"} value={elm} name='first' size="large" color="primary" onClick={() => handleOnChange('first', elm)} className={classes.buttonChoosen}>
                        {elm}
                      </Button>
                    </Grid>
                  )
                })}
                <Grid item xs={12} sm={12} className={classes.center} style={{ marginBottom: '10px' }}><Typography>Tidak sama sekali</Typography></Grid>
              </Grid>
            </Box>
            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
            <TableContainer elevation={0} className={classes.table} component={Paper}>
              <Table aria-label="simple table">
                <TableBody>
                  <TableRow key='questionOne'>
                    {paramZ.map((el) =>
                      <TableCell component="th" scope="row" align="center">
                        <Button variant={values.first === el ? "contained" : "outlined"} value={el} name='first' size="large" color="primary" onClick={() => handleOnChange('first', el)} className={classes.buttonChoose}>
                          {el}
                        </Button>
                      </TableCell>
                    )}
                  </TableRow>
                  <TableRow key='questionTwo'>
                    <TableCell component="th" scope="row" align='center' style={{ padding: "0px 16px" }}>
                      Tidak sama sekali
                    </TableCell>
                    <TableCell component="th" scope="row" colSpan={5} align='center' style={{ padding: "0px 16px" }}></TableCell>
                    <TableCell component="th" scope="row" align='center' style={{ padding: "0px 16px" }}>
                      Sangat fokus
                    </TableCell>
                  </TableRow>
                </TableBody>
              </Table>
            </TableContainer>
            </Box>
            
            <Typography align='justify' className={classes.content}>
              <b>Seberapa fokus Anda memperhatikan artikel di bagian awal penelitian?</b>
            </Typography>
            <Box display={{ xs: 'block', sm: 'block', md: 'none' }}>
              <Grid container>
                <Grid item xs={12} sm={12} className={classes.center} style={{ marginTop: '5px' }}><Typography>Sangat fokus</Typography></Grid>
                {param.map((elm) => {
                  return(
                    <Grid item xs={12} sm={12} className={classes.center}>
                      <Button variant={values.second === elm ? "contained" : "outlined"} value={elm} name='second' size="large" color="primary" onClick={() => handleOnChange('second', elm)} className={classes.buttonChoosen}>
                        {elm}
                      </Button>
                    </Grid>
                  )
                })}
                <Grid item xs={12} sm={12} className={classes.center} style={{ marginBottom: '10px' }}><Typography>Tidak sama sekali</Typography></Grid>
              </Grid>
            </Box>
            <Box display={{ xs: 'none', sm: 'none', md: 'block' }}>
              <TableContainer elevation={0} className={classes.table} component={Paper}>
                <Table aria-label="simple table">
                  <TableBody>
                    <TableRow key='questionOne'>
                      {paramZ.map((el) =>
                        <TableCell component="th" scope="row" align="center">
                          <Button variant={values.second === el ? "contained" : "outlined"} value={el} name='second' size="large" color="primary" onClick={() => handleOnChange('second', el)} className={classes.buttonChoose}>
                            {el}
                          </Button>
                        </TableCell>
                      )}
                    </TableRow>
                    <TableRow key='questionTwo'>
                      <TableCell component="th" scope="row" align='center' style={{ padding: "0px 16px" }}>
                        Tidak sama sekali
                      </TableCell>
                      <TableCell component="th" scope="row" colSpan={5} align='center' style={{ padding: "0px 16px" }}></TableCell>
                      <TableCell component="th" scope="row" align='center' style={{ padding: "0px 16px" }}>
                        Sangat fokus
                      </TableCell>
                    </TableRow>
                  </TableBody>
                </Table>
              </TableContainer>
            </Box>
            
            <Typography align='justify' className={classes.content}>
              <b>Berasal dari negara mana investasi asing yang diberitakan dalam artikel?</b>
            </Typography>
            <FormGroup style={{ marginLeft: "4%" }}>
              {negara.map((el) =>
                <FormControlLabel
                  control={<Checkbox checked={values.third === el} onClick={() => handleOnChange('third', el)} name="third" color="primary" />}
                  label={el}
                />
              )}
            </FormGroup>
            
            <div className={classes.right}>
              <Button disabled={!values.valid} variant="contained" size="medium" onClick={() => {handleSubmit(values.first, values.second, values.third)}} className={classes.button}>
                <b>Lanjut</b>
              </Button>
            </div>
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Penilaian5