import React, { Fragment, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper } from '@material-ui/core';
import AssignmentTurnedInIcon from '@material-ui/icons/AssignmentTurnedIn';

const styles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.primary.main,
    backgroundSize: 'auto',
    width: '100%',
    minHeight: '100vh',
    height: 'auto',
  },
  container: {
    padding: '15% 39% 3% 29%',
    [theme.breakpoints.down('sm')]: {
      padding: '15% 22% 3% 12%',
    },
  },
  paper: {
    padding: '5% 10%',
    width: '100%',
    borderRadius: 18,
  },
  content: {
    paddingLeft: '10%',
    width: '80%',
    marginBottom: '20px'
  },
}))

const Thanks0 = () => {
  const classes = styles();
  
  useEffect(() => {
    window.history.pushState(null, null, window.location.href);
    window.onpopstate = function (event) {
      window.history.go(1);
    };
    window.scrollTo(0, 0);
  }, [])
  
  return (
    <Fragment>
      <div className={classes.root}>
        <div className={classes.container}>
          <Paper className={classes.paper}>
            <Typography align='center' className={classes.content}>Maaf, pengumpulan data untuk studi ini telah selesai.<br/>
            </Typography>
            <AssignmentTurnedInIcon style={{ marginLeft: '44%', color: '#190E5C', fontSize: '60px' }} />
          </Paper>
        </div>
      </div>
    </Fragment>
  )
}
export default Thanks0