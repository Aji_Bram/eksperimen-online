import React from 'react';
import './App.css';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles'
import Router from './Router';

const theme = createMuiTheme({
  palette: {
    primary: { main: '#190E5C' },
    secondary: { main: '#FFFFFF' },
    error: { main: "#BE2E2E" },
  },
  typography: {
    "fontFamily": "\"Roboto\", \"Poppins\", sans-serif",
  }

});

function App() {
  return (
    <MuiThemeProvider theme={theme}>
      <Router />
    </MuiThemeProvider>
  );
}

export default App;

//#00B3B3 - Tiffany Blue