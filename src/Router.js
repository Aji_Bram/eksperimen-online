import React, { Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

import LandingPage from './pages/LandingPage';
import Thanks0 from './pages/Thanks0';
import Instruction from './pages/Instruction';
import Stimulus from './pages/Stimulus';
import Penilaian1 from './pages/Penilaian1';
import Penilaian2 from './pages/Penilaian2';
import Penilaian3 from './pages/Penilaian3';
import Penilaian4 from './pages/Penilaian4';
import Penilaian5 from './pages/Penilaian5';
import Information from './pages/Information';
import Closing from './pages/Closing';
import Lottery from './pages/Lottery';
import Thanks1 from './pages/Thanks1';
import Thankyou from './pages/Thankyou';

const Router = () => {
	return (
		<Fragment>
			<Switch>	
				<PublicRoute exact path='/' component={LandingPage} />
        <PublicRoute exact path='/thanks-0' component={Thanks0} />
				<PublicRoute exact path='/instruction' component={Instruction} />
				<PublicRoute exact path='/stimulus' component={Stimulus} />
				<PublicRoute exact path='/penilaian-1' component={Penilaian1} />
				<PublicRoute exact path='/penilaian-2' component={Penilaian2} />
				<PublicRoute exact path='/penilaian-3' component={Penilaian3} />
				<PublicRoute exact path='/penilaian-4' component={Penilaian4} />
				<PublicRoute exact path='/penilaian-5' component={Penilaian5} />
				<PublicRoute exact path='/information' component={Information} />
				<PublicRoute exact path='/closing' component={Closing} />
				<PublicRoute exact path='/lottery' component={Lottery} />
				<PublicRoute exact path='/thanks-1' component={Thanks1} />
      </Switch>
    </Fragment>	
  )
}

const PublicRoute = ({ component: Component, props:cProps,  ...rest }) => (
	<Route {...rest} render={
		props=>(<Component  {...props} {...cProps}  />)
	}
	/>
)

export default Router;
