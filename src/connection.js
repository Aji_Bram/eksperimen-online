var bcrypt = require('bcryptjs');
var dateFormat = require('dateformat');
const Pool = require('pg').Pool
const pool = new Pool({
  user: process.env.PG_USER,
  host: process.env.PG_HOST,
  database: process.env.PG_DATABASE,
  password: process.env.PG_PASSWORD,
  port: process.env.PG_PORT_DB,
})

pool.connect(function (err) {
  if (err) {
    throw err;
  }
  let createRespondenTable = `CREATE TABLE IF NOT EXISTS RESPONDEN(respid integer, choosennum integer[]);`

  pool.query(createRespondenTable, function (error, results, fields) {
    if (error) {
      throw error;
    }
    var val = []
    var setValue = function (value) {
      val = value;
    }
    let query = `SELECT * FROM RESPONDEN;`
    pool.query(query, function (error, results) {
      if (error) {
        res.send({
          auth: false,
          message: error
        })
      }
      setValue(results);
      var string = JSON.stringify(val)
      var responden = JSON.parse(string)

      if (responden.rowCount === 0) {

        let createResponden = `INSERT INTO RESPONDEN(respid, choosennum) VALUES (1, ARRAY[0]);`
        pool.query(createResponden, function (error, results) {
          if (error) {
            console.log(error);
          }
        })
      }
    });
  })
});